# TeachMyselfCS

Following the guide set out at [Teach Yourself CS](https://www.teachyourselfcs.com)

|Subject | Recommended book | Recommended videos | Progress | 
| --- | --- | --- | --- | 
|Programming | Structure and Interpretation of Computer Programs | Brian Harvey’s Berkeley CS 61A | |
|Computer Architecture | Computer Systems: A Programmer's Perpsective | [Berkeley CS 61C]() | |
|Algorithms and Data Structures | The Algorithm Design Manual | Steven Skiena’s lectures | |
|Math for CS | Mathematics for Computer Science | Tom Leighton’s MIT 6.042J | |
|Operating Systems | [Operating Systems: Three Easy Pieces](https://ostep.org) | [Berkeley CS 162](https://www.youtube.com/playlist?list=PLRdybCcWDFzCag9A0h1m9QYaujD0xefgM) | In Progress |
|Computer Networking | Computer Networking: A Top-Down Approach | Stanford CS 144 | |
|Databases | Readings in Database Systems | [Joe Hellerstein’s Berkeley CS 186](https://archive.org/details/UCBerkeley_Course_Computer_Science_186) | On hold |
|Languages and Compilers | Crafting Interpreters | Alex Aiken’s course on edX | |
|Distributed Systems | Designing Data-Intensive Applications - Martin Kleppmann | MIT 6.824 | 	|

