# Lecture 01-01: The Security Mindset

## Why Cyber Security?

- We worry about security when we have something of value and there is a risk that it could be harmed
- What is of value when we are talking about cyber security?
  - We all store a lot of sensitive data
    - If this data is misused, criminals can profit from it (at the expense of the individual)
  - Critical infrastructure (e.g. smart grids) rely on cyber systems 
    - Whoever controls the grid controls the community infrastructure
  - Business and governmental information is often stored in an internet-accessible location
    - Unauthorized access could be economically or politically dangerous

### Cyber assets at risk

- How do we understand the risk to our online information and systems?
  - We need to develop a security mindset:
    - Getting a feel for the threats/bad actors, the vulnerabilities they can exploit and the attacks that they may carry out
  - **Threat actor**: someone who wants to do harm to the system
    - Often cybercriminals, hacktivists or nation states
  - **Vulnerabilities and Attacks**
    - Threat actors exploit vulnerabilities to launch attacks
