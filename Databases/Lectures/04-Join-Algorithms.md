UP Berkeley CS186 - Introduction to Database Systems
2015 Recording

[Lectures Link](https://archive.org/details/UCBerkeley_Course_Computer_Science_186/Computer+Science+186+-+2015-01-20-dY48_UZhvhw.mkv)


- Rendezvous: two (or more) things being coresident in memory at the same time
- Grouping/Aggregation is one type of rendezvous
  - Groups of matching items within one file
- Joining is the other main type of join algorithm
  - Combinations of items from different files/tables

### Cross Product
- Given two collections, `R` and `S`
- `R x S`: all pairs `{r, s}` of items in `R, S`
  - a.k.a. Cartesian Product
- This is the largest join we can make from two sets 

### "Theta" Join

- `R x_theta S`: All pairs `{r, s}` where `Theta(r,s)`
  - Theta is some predicate that compares `r` tuples with `s` tuples
- Common case: EquiJoin
  - Theta is an equality test
  - One side of the equality is a key
    - E.g. Enrollments.StudentId = Students.Id
    - Students.Id is the key
    - This is like doing "lookups" in the students table

### Some examples:

- Notation:
  - [R] = number of pages required to store R
  - p_r = number of records per page of R
  - |R| = number of records in R (= cardinality)
  - [R] * p_r = |R|
  - Assuming 4KB pages

- Schema:
  - `sailors(sid[key]: integer, sname: string, rating: integer, age: real)`
    - key is the sailor ID
    - 50 bytes per tuple, 80 tuples per page, 500 pages
    - [S] = 500, p_s=80
  - `reserves(sid[key]: integer, bid[key]: integer, day[key]: dates, rname: string)`
    - Composite key, from sailorID/boatID/day
    - 40 bytes per tuple, 100 tuples per page, 1000 pages
    -[R]=1000, p_r=100
- Common cost metric is "number of IOs issued to disk"
  - IO is way more expensive than anything else
  - Sequential IO is much faster than random IO on magnetic disks
  - Sometimes we want to distinguish between sequential/random IO, but not right now

#### JOINs

```SQL
SELECT *
FROM reserves r1, sailors s1
WHERE r1.sid = s1.sid
```

- Joins are very common
- `R x S` is very large
  - That means that `R x S` followed by a filter is slow
- Naive join approach:
  - "blow up" the entire cartesian space
  - filter out all the tuples that don't fit the filter
  - pro tip: it's bad! (it's O(n)^2)
- We have many ways to reduce join cost:
  - Nested-loops join
  - Index-nested loops join
  - Sort-merge join
  - Hash joins
  - \+ many more!

##### Nested Loops
```
foreach record r in R do
  foreach record s in S do
    if theta(r,s) then add <r, s> to result
```
- Cost:
  - [R] \* [p_r] \* [S] + [R]
  - 1000 * 100 * 500 + 1000 IOs
  - What does this mean for 10ms/IO?
    - 138 hours...
  - What if the smaller relation (S) was "outer"
      - [S] \* [p_s] \* [R] + [S]
      - Very mild effect as the dominating factor is n^2
  - What assumptions have we made here?
    - All disk IOs are equal
    - We're assuming we have some level of control over the hardware, but this is not necesarrily accurate - there's the OS and hardware providing abstractions to us
      - That being said, most OSes have a flag to get back some of the control
  - What's the cost if one relation can fit entirely in memory?
    - [R] + [S]!

##### Page-oriented Nested Loops Join

```
foreach page b_r in R do
  foreach page b_s in S do
    foreach record r in b_r do
     foreach record s in b_s do
       if theta(r,s) then add <r,s> to result
```
- This means we read [R] once, and read [S] once per page of R
  - `[R]*[S]+R`

#### Chunk Nested Loops Join

- Page-oriented nested loops join doesn't use all of our available memory
- Instead, (given `B` pages of memory) we read in `B-2` pages of `R`
  - Read in one page of `S`
  - For each tuple in the `B-2` pages of `R`, loop through the tuples in `S`
  - Cost: [R] + [R] * [S] / [B-2]
  - This shaves off a factor of `B-2` from page-oriented nested loops
  - It's slightly better to use the smaller side of the join as the outer component, but not much

#### Index Nested Loops Join

```
foreach tuple r in R do
  foreach tuple s in S where r_i == s_j do
    add <r,s> to result
```

- Couldn't we use some kind of index on these first, instead of scanning all of S?
- Cost becomes [R] + [R] \* p_r \* cost to find matching S tuples
  - cost to find matching tuples will come from a tree-like data structure, so it is probably approximately log(|S|)

#### Sort-Merge Join
1. Sort `R` on join attribute(s)
2. Sort `S` on join attribute(s)
3. Scan sorted `R` and `S` in tandem to find matches

- Cost:
  - Sort R + Sort S + ([R] + [S])
    - Worst case, last term is [R]*[S] 

- Other factors:
  - You could do the join in the final merging pass of the sort!
  - If you've got enough memory, you can:
    - Read R and write out sorted runs (pass 0)
    - Read S and write out sorted runs (pass 0)
    - Merge R-runs and S-runs while finding R x S matches

- Sort-Merge is very good if:
  - one/both inputs is already sorted on the join attributes
  - the output needs to be sorted on the join attributes

#### Hash Join

- Generate hashes for R
- Generate hashes for S
- Bring the smaller relation in to memory (e.g. `R`) as a hash table, then bring in batches of `S` as required
- Cost: similar to sort-merge join

#### Hash join vs Sort-Merge join
- Sorting pros:
  - Good if input is already sorted, or if output needs to be sorted
  - Not sensitive to data skew/bad hash functions
- Hashing pros:
  - Can be cheaper due to hybrid hashing
  - For join: # passes depends on the size of the smaller relation
  - Good if input is already hashed or the output needs to be hashed

### Summary
- Nested loops join
  - Works for arbitrary theta (comparison function) 
  - Make sure to chunk your memory!
- Index nested loops join
  - For equi-joins (theta is equality test, one side of test is a key)
  - Works when you already have some sort of index on one side
- Sort/Hash join
  - Again, for equi-joins
  - Don't need an index!
- No objective winners
  - Different cost model means the query optimizer actually needs to do some work

