UP Berkeley CS186 - Introduction to Database Systems
2015 Recording

[Lectures Link](https://archive.org/details/UCBerkeley_Course_Computer_Science_186/Computer+Science+186+-+2015-01-20-dY48_UZhvhw.mkv)

- Start with single table queries
  - Basic SQL
  - Query Executor Architecture

## Relational tables
  - *Schema* is fixed (think: metadata)
    - Attribute names, atomic types
    - There is an ordering defined on the columns - its the order you specify when creating the table. Order not used much though
    - e.g. `students(name text, gpa float, dept text)` defines:
      - `students` table with three columns
        - `name`, a text column
        - `gpa`, a float column
        - `dept`, a text column
    - You *can* alter a table over time (e.g. add/drop columns), but it's usually safe to assume the structure is fixed
      - Similar to a type definition (in a strongly typed language) - the type can change but not without a bit of work
  - *Instances* can change
    - instance: a *multi*set of rows (or tuples). Duplicates are allowed.
    - Every instance must fit the schema
    - E.g: 
      - ('Bob Snob', 3.3, 'CS')
      - ('Bob Snob', 3.3, 'CS')
      - ('Rupert Hardinger', 3.2, 'CS')
      - ('Mary Contrary', 3.8, 'CS')

## Basic Single-Table Queries

```SQL
    SELECT [DISTINCT] <column_expression_list>
    FROM <single_table>
   [WHERE <predicate>]
   [GROUP BY <column_list>
   [HAVING <predicate>] ]
   [ORDER BY <column_list>];
```
- Things in [ ] are optional

#### Simplest version:

```SQL
SELECT S.name, S.gpa
FROM students S
WHERE S.dept = 'CS';
```
- Returns all tuples in students that satisfy `students.dept = 'CS'`
  - For each tuple in output, return the columns `students.name`, `students.gpa`


- `SELECT *` is just a macro for listing out all of the column names
- We can also remove duplicates by using `SELECT DISTINCT`
  - This filtering happens just before outputting

### Adding some ordering

```SQL
SELECT DISTINCT S.name, S.gpa, S.age*2 as a2
FROM Students S
WHERE S.dept = 'CS'
ORDER BY S.gpa, S.name, a2;
```

- `ORDER BY` clause specifies ordering of the output
  - Lexicographic ordering - like dictionary ordering but with fields instead of letters
  - Must refer to columns in the output
  - Can specify order direction - ascending order by default
  - Can mix and match, lexicographically
    - `ORDER BY S.gpa DESC, S.name ASC`

### Aggregation

```SQL
SELECT AVG(S.gpa)
FROM Students S
WHERE S.dept = 'CS'
```

- Before producing the output, compute a summary (or *aggregate*) of some arithmetic expression
- Produces 1 row of output
- Other alternatives: `SUM, COUNT, MAX, MIN`
- Can use `DISTINCT` inside the aggregate function - two different expressions
  - Distinct evaluates whether all fields are equal
  - `SELECT COUNT(DISTINCT S.name) FROM Students S`
    - Removes duplicates before counting
  - `SELECT DISTINCT COUNT(S.name) FROM Students S`
    - Removes duplicates *after* counting

- Now with some aggregation:
```SQL
SELECT AVG(S.gpa), S.dept FROM Students S GROUP BY S.dept;
```

- This will sort of perform separate `SELECT` statements for each `DISTINCT S.dept`.
- If we have 3 departments (CS, BUSINESS, MEDICINE), we will have 3 rows of output
- Any columns we `SELECT` either needs to be:
  - In the `GROUP BY` clause
  - Inside an aggregate function

```SQL
SELECT AVERAGE(S.gpa), S.dept
FROM Students s
GROUP BY S.dept
HAVING COUNT(*) > 5
```
- We can also filter after the group by - so in this query we only hit departments with more than 5 people in them
- `WHERE` is applied before aggregation, `HAVING` is applied afterwards
  - `HAVING` can only appear after a `GROUP BY` clause
  - if there is no `GROUP BY` clause there is implicitly 1 group

## Query Processing Architectures

- Layers of DB Query processing:
  - Query Optimizing and Execution
  - Relational Operators
  - Files and Access Methods
  - Buffer Management
  - Disk Space Management

### Query Optimizer

- Query Optimizer translates SQL into a special internal "language"
  - "query plan"
- Query executor is an *interpreter* of query plans
- Think of query plans as "blobs and arrows" dataflow diagram
  - Each blob is a relational operator
  - Edges represent the flow of tuples (columns)
  - For single table queries, these are straight line graphs:

```SQL
SELECT DISTINCT name, gpa FROM STUDENTS
```
Becomes 

 ![](img/lec03_01-query-optimizer.png)

 - Scan students table, pass (name, gpa) tuples up to next node in graph
 - Sort records as they come in to the node, pass them to the next node
 - Throw out repeated records as they come out of the sort

- In UNIX speak, it's similar to:
  - `cat file | sort | uniq`

### Iterators

- Relational operators are all derived from `iterator`:

```Cpp
class iterator {
    void init();
    tuple next();
    void close();
    iterator &inputs[];
    bool isHashed;
    // additional state here
}
```

- Edges in the graph are specified by inputs (max of 2, usually)
- Encapsulation: any iterator can be an input for any other
- When subclassing, different iterators will have different kinds of state information

- All calls are synchronous

#### Example: Sort Iterator

```Cpp
class Sort extends iterator {
  void init();
  tuple next();
  void close();
  iterator &inputs[1];
  int numberOfRuns;
  DiskBlock runs[];
  RID nextRID[];
}
```

- `init()`
  - generate the sorted runs on disk
  - allocate the `runs[]` array, fill with pointers to disk
  - init `numberOfRuns`
  - allocate `nextRID[]`, init to `null`
- `next()`
  - `nextRID[]` tells us where we're up to in each run
  - find the next tuple to return based on the `nextRID[]`
  - advance the corresponding `nextRID[]` entry
  - return tuple (or EOF if no tuples remain)
- `close()`
  - free up the used memory

#### SORT GROUP BY
- Requires the input to be sorted beforehand
- The Sort iterator ensures all of the output tuples are returned in sequence
- The Aggregate iterator keeps running info ("transition values") on aggregate functions in the SELECT list, per group
  - E.g. for COUNT, it keeps count-so-far. for SUM, sum-so-far. for AVG, sum-so-far and count-so-far
- As soon as the Aggregate iterator sees a typle from a new group
  - It produces output for the old group based on the aggregate function
  - It resets its run info
  - It updates the run info with the new tuple's info

#### HASH GROUP BY (naive)
- Requires the input to be hashed beforehand
- Hash iterator ensures all tuples are output in batches
- The Aggregate iterator keeps running info ("transition values") on aggregate functions in the SELECT list, per group
  - E.g. for COUNT, it keeps count-so-far. for SUM, sum-so-far. for AVG, sum-so-far and count-so-far
- As soon as the Aggregate iterator sees a typle from a new group
  - It produces output for the old group based on the aggregate function
  - It resets its run info
  - It updates the run info with the new tuple's info

#### HASH AGGREGATE (better!)
- Combine the summarization into the hashing process!
- During the ReHash phase, instead of storing the raw tuples store `<GroupValues, TransVals>`
- When we want to insert a new value into the hash table:
  - If we find a matching `GroupVals`, just update the `TransVals` as required
  - Else insert a new `<GroupValues, TransVals>` pair
- Benefit???
  - We only have to hash the number of distinct values in the GROUP BY columns
    - Not the number of tuples!
  - Number of distinct values is probably "narrower" than the number of tuples