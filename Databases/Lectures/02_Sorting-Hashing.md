UP Berkeley CS186 - Introduction to Database Systems
2015 Recording

[Lectures Link](https://archive.org/details/UCBerkeley_Course_Computer_Science_186/Computer+Science+186+-+2015-01-20-dY48_UZhvhw.mkv)

# Out of core[^1] sorting and hashing
[^1] : memory
## Sorting
- Rendezvous
  - Eliminate duplicates
  - Summarizing groups of items
  - Gets similar objects in the same place at the same time
- Ordering
  - Sometimes we care about the ordering of our output
  - E.g. might want to return results in decreasing order of relevance
- Fundamentals:
  - Sort-merge-join algorithm involves sorting
  - First step in bulk loading tree indexes (ordering)
- Sort 100GB of data using 1GB of RAM
  - Why don't we want to use virtual memory?
    - Lots of pagefaults!
    - Lots of random access to virtual memory
    - Virtual memory system is a nice abstraction for programmers, but is awful for large data tasks
    - Need more methodical algorithm design to account for:
      - Some memory is slow (HDD/SSD)
      - Some memory is medium speed (RAM)
      - Some memory is very fast (CPU caches)

## Disks and Files
- Lots of databases still use mechanical disks
- No pointer dereferences - instead we have an API
  - READ: Transfer a `page` of data from disk to RAM
  - WRITE: Transfer a `page` of data from RAM to disk
  - Both API calls are expensive (especially with magnetic disks)!
- An explicit API can be a good thing though!
  - Well designed API can increase memory safety

## Storage Hierarchy

- From Small/Fast to Big/Slow:
    - CPU Registers
    - On chip cache (L1/L2/L3)
    - On board cache
    - RAM - data currently in use
    - SSD - varies by deployment - sometimes used for db, sometimes for a cache
    - HDD - main database, backups, logs
    - Tapes - backups, rarely used

## How far away is data?

Order of Magnitude | Media  | Distance
---|---|---
10^9    | Tape/Optical Robot    | Andromeda
10^6    | Disk                  | Pluto
100     | Memory                | Bunbury
10      | On board cache        | This building
2       | On chip cache         | This room
1       | Registers             | My head

## Hardware bottom line/implications
- "Very Large DBs": relatively traditional
  - Disk still the best $/MB ratio by a few orders of magnitude
  - SSDs improve performance but also *performance variance* (more consistent)
- Smaller DB story is changing quickly
  - Entry cost for disks is not insignificant, flash can win at low end
  - Many interesting dbs will fit in RAM
- HW storage tech changing

## Better performance: Double Buffering
- Main thread applies f(x) to one pair of I/O buffers
- Second thread fills/drains second pair of I/O buffers
- When main thread is ready for a new buffer, swap them!
- Assume that we are using this technique all the time 

## Sorting and Hashing: Formal Specs
- Given:
  - A file `F`, containing multiset records `R` and consuming `N` blocks of storage
  - Two "scratch" disks, each with >>`N` blocks of free storage - disk space is not a factor here
  - A fixed amount of space in RAM (<`N`), equivalent to `B` blocks of disk
- Sort:
  - Produce an output file `F_s`, with contents `R` stored in an order specified by the sorting criterion
- Hash
  - Produce an output file `F_h`, with contents `R` arranged on disk such that matching records are always "stored consecutively"
    - aka: No two records that are "equal" in sort order are separated by a record that is "greater" or "smaller" in sort order
    - Acceptable: [2, 2, 2, 2, 2, 5, 5, 5, 5, 1, 1, 1, 65, 65, 65, 65, 65, 65, 4, 4, 4, 4] 
    - Unacceptable: [2, 2, 4, 2, 2]

### Sorting: 2 way

- Pass 0 (conquer):
  - Read a page, sort the page, write the page
  - Only uses one buffer page
  - Repeated "batch job"
- Pass 1, 2, 3, ... (merge):
  - Take two pages from previous pass
  - Merge pairs of pages into runs twice as long as the previous pass
  - Requires 3 buffer pages (two input, one output)
- Streaming algorithm!
  - Takes log_2(`N`) + 1 passes
  - Total (IO) cost is `2*N(log_2(N)+1)`

#### Memory requirements

- How much data can we sort in two runs?
  - Each sorted run after pass0 is size `B`
  - Can merge up to `B-1` sorted runs in pass1
  - That means we can sort files of size `B(B-1)` in two runs
  - Alternatively, we can sort `N` pages of data in approximately `sqrt(N)` pages of RAM
  - Ratio of `N:sqrt(N)` is common with disk:RAM (1TB disk:32GB of RAM) - seems off for consumer grade

#### Sorting algorithm selection

- Quicksort is a quick way to sort in memory!
- Tournament sort/heapsort/replacement selection has some nice properties though...
  - Keep two heaps in memory: H1 and H2
  ```python
    h1 = disk.read(B-2) # read records from disk, store in h1
    while(records left):
      m = h1.removeMin() # remove the min value of h1, store in output buffer
      if (h1.size() > 0):
        r = disk.read(1) # read a single record r
        if (r < m):
          h2.insert(r)
        else:
          h1.insert(r)
      else:
        # switch the input buffers
        h1 = h2
        h2.reset()
        # start new output run
  ```
- Effectively, 
  - Best case with heap sort is do everything in a single run
  - Worst case with heap sort is the same performance as quick sort
  - Quicksort may be faster in theory, but heapsort will have longer runs
    - Longer runs mean that data is more likely to be in a cache
    - Longer runs also mean fewer passes

### Hashing

- Often we don't really care about order (removing duplicates, forming groups)
- Often we just want to rendezvous matches - same place at same time
- Hashing does this, and can be cheaper than sorting! (in terms of IO)
- But how do we do this out-of-memory

#### Divide and Conquer

- Streaming partition (divide)
    - Use a hash function `h_p` to stream records to disk partitions
    - All matches end up in the same partition
    - Streaming algorithm to create partitions on disk
    - `h_p` should be fairly coarse grained
- Re-hash (conquer)
  - Read partitions in to RAM hash table one at a time using a new hash function `h_r`
  - Go through each bucket of this hash table to make records rendezvous in RAM

- E.g. we want rendezvous of strawberries from a dataset of ` {apples, oranges, strawberries, carrots, bananas}`
  - `h_p(x) = colour(x)`
    - This gives us three buckets:
      - `b1 = {apples, strawberries}`
      - `b2 = {oranges, carrots}`
      - `b3 = {bananas}`
  - Persist the buckets to disk, then apply `h_r = fruit(x)` to our buckets, one at a time:
    - `map(h_r, b1) => {apples}, {strawberries}`
    - This gives us all the strawberries in memory at once!
- Cost is 4*N IOs - two reads, two writes
- How big of a table can we hash in two passes?
  - `(B-1)` partitions from pass 1
  - Each partition should be no larger than `B`
  - So again, we get `B(B-1)` or approximately a table of `N` pages in `sqrt(N)` pages of RAM
- **Assumes that the hash function distributes records (approximately) evenly amongst the buckets in step 1**

### Cost of external sorting

- Sorting and Hashing have similar IO costs
  - Sorting is conquer and merge
  - Hashing is divide an conquer
  - These are more or less the same steps, in different orders
- Hashing:
  - Read sequentially, write randomly (in divide step)
  - Read sequentially, write sequentially (in conquer step)
- Sorting:
  - Read sequentially, write sequentially (in conquer step)
  - Read sequentially, write randomly (in merge step)
- They're more or less the same! Only the timing of the random IO is altered
  - Hashing is sensitive to skew in the data - some partitions may be significantly longer than others, if a partition is longer than your available memory then you will have a problem

## Parallelism

### Hashing
- Phase 1: partition data across machines
  - Stream to network as it's scanned
  - Decide on machine for the record based on a hash function (`h_n`)
- Proceed as normal once each machine has a partition it can work with

### Sorting
- Pass 0: partition data over machines
  - Which machine for this record? Partition by value
    - `([-inf, 0], [1, 10], [11, 20], [21, 30], [31, inf])`
    - You need to try and partition evenly, but you haven't seen the data so you don't know what the distribution of the data is
    - Sample the data first! Get an idea of the underlying distribution

## Which is better? Sorting or Hashing?

- Sorting:
  - You need to sort if someone told you to sort
  - Best case (already sorted data), heap sort takes a single pass
  - Sorting isn't vulnerable to data skew (if sorting on one node)
- Hashing:
  - Eliminating duplicates? Scales with # of unique values - not number of items
