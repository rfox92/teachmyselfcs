UP Berkeley CS186 - Introduction to Database Systems
2015 Recording

[Lectures Link](https://archive.org/details/UCBerkeley_Course_Computer_Science_186/)

## Database vs DBMS

- Database is a large collection of structured data

- Database management system (DBMS) is software that stores, manages, facilitates access to databases
    - Used to refer to relational databases, transactions
    - Tech behind RDBMS are more or less the same
    - The tech is being applied in different ways, remixed to handle:
      - Hardware 
        - changed storage media 
        - used to be on HDDs, now stored on SSD or in RAM
      - Volume
        - Works against the hardware point
        - Large volume means we might have to still work with HDDs
        - Tradeoff between efficiency, scalability, ease of management
      - Wider use cases

## Is an OS a DBMS?

- ✅ Data stored in RAM 
  - All programming languages offer this
  - RAM is fast, random access
- ✅ Operating Systems include File Systems
  - Manage files on a persistent disk
  - Can open/read/seek/close files
  - Security! File permissions!
  - Slow...
  - Not really random access...
  - OS file system API is shared between processes - RAM is process-isolated

- Thought experiment: 
  - Two users edit the same file
  - Both users save the file at the same time
  - Whose changes win?
  - Who knows! Any outcome could happen.

- How do you work against an API that has an undefined outcome?
  - *very carefully*

- What do we want in a DBMS that an OS doesn't provide?
  - Clear API contracts regarding data
    - Concurrency management, data replication/recovery
  - Simple ad-hoc queries - high level language
  - Efficient/scalable bulk processing
  - A good data model

## Dealing with Big Data

- Von Neumann Machine:
    1. Read something from memory
    1. Do something
    1. Write something to memory
    1. GOTO 1.
  - Doesn't deal well with large amounts of data
  - SQL and MapReduce are a few instances of non-Von Neumann architectures


### Basic Patterns for Big Data

- If the data doesn't fit in memory, you may as well do it on two computers
- **Simplifying assumption:** We only have to deal with sets and relations - no ordered collections/types
  - No intrinsic ordering of our data, so we can reorder our operations as we like
  - "Unordered handling of unordered data will set you free"
    - We can order things to our liking: e.g. based on cache locality, rendezvous[^1]
    - We can perform batch processing: choose batch sizes based on memory hierarchy[^2], choose batch contents based on anything we want, postpone arbitrary data that doesn't fit in our current batch
    - We can tolerate non-deterministic ordering (e.g. for parallel execution results, efficiency)

[^1]: Making sure two items that appear at the same time do so
[^2]: E.g. to make sure an entire batch fits in L1/L2/L3 cache

#### Streaming Computation Model

- Data often appears in *an* order, but not any *particular* order
- Say we want to process 100GB of data on a laptop
  - We want to compute some arbitrary f(x) on each record and write out the result somewhere
  - Challenge is to both minimize RAM usage but also reduce the number of `read` and `write` syscalls, as they have overhead
- Approach: 
  - Read a sizeable chunk of Input to an *Input Buffer*
  - Write f(x) for each item to an *Output Buffer*
  - When input buffer is consumed, read another chunk
  - When output buffer is consumed, write it to Output
- Result:
  - Reads/writes not co-ordinated
  - If f(x) == Compress(x) then you do many read operations per write operation
  - If f(x) == Decompress(x) then you do many write operations per read

Unix pipes do this!
  - STDIN/STDOUT streams
  - Streaming Unix utilities get/put lines (chunks based on `\n` delimiters)
  - If you use `|` then the OS does the chunking for you

#### Divide and Conquer

- Rendezvous:
  - Some algorithms need certain groups of items to be in memory simultaneously
    - E.g. the JOIN algorithm needs to make sure that tuples from one table are co-resident in memory with tuples from another table
  - Our system doesn't guarantee that we will/won't have any two items in memory at the same time

- Out of core[^3] algorithms orchestrate Rendezvous
- Typical memory allocation:
  - Assuming N chunks of RAM available
  - Use 1 chunk of RAM for reading/input
  - Use 1 chunk of RAM for writing/output
  - You get N-2 chunks for orchestrating rendezvous

[^3]: Core being an old term for memory (e.g. `segfault: core dumped`)

- Typical pattern: 
  - Streamwise *divide* the data in to M/(N-2) "megachunks", process each, then write off to disk
  - Perform a streamwise algorithm over conquered "megachunks"
    - Streaming must ensure rendezvous (done in phase1)


