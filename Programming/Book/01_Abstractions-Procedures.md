#  Chapter 01 Building Abstractions with Procedures


We are concerned with the idea of *computational processes*.

Computational processes are abstract beings that inhabit computers.

As they evolve, processes manipulate abstract things called *data*.

The evolution of a process is directed by a pattern of rules called a *program*

People create programs to direct processes.

***In effect, we conjure the spirits of the computer with our spells.***

### Why Learn Lisp?

- Language has unique features for studying important programming constructs
  - **Lisp *procedures* (processes) can be represented as data**
  - Powerful program design techniques that rely on blurring the line between "passive" data and "active" processes
  - The ability to blur the line between code and data also allows us to write programs that manipulate other programs as data (e.g. interpreters and compilers)
- Programming in Lisp is *fun*

## 1.1 Elements of Programming

Important to understand the way a given language lets us combine simple ideas to more complex ones. Every powerful language gives us three classes of tools here:

- ***Primitive expressions*** - the smallest entities the language cares about
- ***Means of combination*** - creating compound elements from simpler elements
- ***Means of abstraction*** - naming compound elements, allowing them to be manipulated as units

We deal with two types of elements: *procedures* and *data*. Data is "stuff we want to manipulate" and procedures are descriptions of rules for manipulating data.

Any powerful programming language should be able to:
  - describe primitive {data|procedures}
  - combine primitive {data|procedures}
  - abstract combinations of primitive {data|procedures}

  ### 1.1.1 Expressions

  