# Project 1: Implement basic chips


- Helper chips are not required
- Aim for as few chip parts as possible
- Multi-bit buses are indexed right to left


- Implement chips in the given order
- ✅ Elementary logic gates:
  - ✅ Not 
  - ✅ And
  - ✅ Or
  - ✅ Xor
  - ✅ Mux
  - ✅ Dmux
- ✅ 16-bit variants
  - ✅ Not16
  - ✅ And16
  - ✅ Or16
  - ✅ Mux16
- ✅ Multi-way variants
  - ✅ Or8Way
  - ✅ Mux4Way16
  - ✅ Mux8Way16
  - ✅ Dmux4Way
  - ✅ Dmux8Way