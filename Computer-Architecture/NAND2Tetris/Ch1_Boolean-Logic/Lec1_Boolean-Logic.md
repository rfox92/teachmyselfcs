# Lecture 1: Boolean Functions and Gate Logic

[Coursera Link](https://www.coursera.org/learn/build-a-computer/supplement/gaUOI/module-1-boolean-functions-and-gate-logic-roadmap)

## 1.1 Boolean Logic

Some logic/truth tables

x | y | x AND y | x OR y | NOT x
---|---|---|---|---
0|0|0|0|1
0|1|0|1|1
1|0|0|1|0
1|1|1|1|0

### Boolean Functions
f(x,y,z) = (x AND y) OR (NOT(x) AND z)

x | y | z | f
---|---|---|---
0|0|0|0
0|0|1|1
0|1|0|0
0|1|1|1
1|0|0|0
1|0|1|0
1|1|0|1
1|1|1|1

### Boolean Identities

- Commutative Laws
  - (x AND y) = (y AND x)
  - (x OR  y) = (y OR  x)
- Associative Laws
  - (x AND (y AND z)) = ((x AND y) AND z)
  - (x OR  (y OR  z)) = ((x OR  y) OR  z)
- Distributive Laws
  - (x AND (y OR  z)) = (x AND y) OR  (x AND z)
  - (x OR  (y AND z)) = (x OR  y) AND (x OR  z)
- De Morgan Laws
  - NOT(x AND y) = NOT(x) OR  NOT(y)
  - NOT(x OR  y) = NOT(x) AND NOT(y)
- Idempotence Law
  - (x AND x) = x
  - (x OR  x) = x

## 1.2 Boolean Function Synthesis

- Previously, we've gone from a function definition to a truth table
- Now we want to consider the opposite - we have the truth table, but we want to know what logical units we need and how to compose them

x|y|z|f|
---|---|---|---
0|0|0|1
0|0|1|0
0|1|0|1
0|1|1|0
1|0|0|1
1|0|1|0
1|1|0|0
1|1|1|0

- First, we only consider the rows where f == 1

x|y|z|f|f_1|f_2|f_3
---|---|---|---|---|---|---
0|0|0|1|1|0|0
0|1|0|1|0|1|0
1|0|0|1|0|0|1

- Then, we compose a function that is only true when a single line is true
  - `f_1 = (NOT(x) AND NOT(y) AND NOT(z))` => only 1 for line 1
  - `f_2 = (NOT(x) AND y      AND NOT(z))` => only 1 for line 2
  - `f_3 = (x      AND NOT(y) AND NOT(z))` => only 1 for line 3
- Now we want to combine these functions together to a single expression
  - If we `OR` these three expressions together, we get a single boolean expression that would achieve our goal:
  - `(NOT(x) AND NOT(y) AND NOT(z)) OR (NOT(x) AND y      AND NOT(z)) OR (x      AND NOT(y) AND NOT(z))`
  - This expression can be simplified!
    - First, our top two clauses contain `NOT(y)` and `y` combined with `NOT(x) AND NOT(z)`
    - This means they can be combined and remove the `y` to give us `(NOT(x) AND NOT(z)) OR (x AND NOT(y) AND NOT(z)`
    - `NOT(z)` appears on either side of the `OR`, so it can be factored out to give us `NOT(z) AND (NOT(x) OR NOT(y))`
    - Overall, finding the shortest expression for an arbitrary truth table is an NP-hard problem

## 1.3 Logic Gates

- How do we implement boolean functions using hardware?
- We use logic gates!
- A logic gate:
  - Standalone chip
  - Can be elementary (Nand, And, Or, Not, ...)
  - Can be composite (Mux, Adder, ...)
    - Made up of multiple elementary (or composite!) logic gates


### Elementary Logic Gates

#### Nand
![](../img/13_logical_nand.png)
- Takes two inputs (a,b), returns a single output (out)
- Functional spec:
  - `if ( a == 1 and b == 1 ) then out = 0 else out = 1`
- Truth table:

a | b | out
---|---|---
0|0|1
0|1|1
1|0|1
1|1|0

#### And
![](../img/13_logical_and.png)
- Takes two inputs (a,b), returns a single output (out)
- Functional spec:
  - `if ( a == 1 and b == 1) then out = 1 else out = 0`
- Truth table:

a | b | out
---|---|---
0|0|0
0|1|0
1|0|0
1|1|1

#### Or
![](../img/13_logical_or.png)
- Takes two inputs (a,b), returns a single output (out)
- Functional spec:
  - `if ( a == 1 or b == 1) then out = 1 else out = 0`
- Truth table:

a | b | out
---|---|---
0|0|0
0|1|1
1|0|1
1|1|1

#### Not
![](../img/13_logical_not.png)
- Takes one input (a), returns a single output (out)
- Functional spec:
  - `if (a == 1) then out = 0 else out = 1`
- Truth table

a | out
---|---
0|1
1|0

### Composite Logic Gates

#### 3-Way AND
![](../img/13_logical_3way-and.png)
- Takes three inputs (a,b,c), returns a single output (out)
- Functional spec:
  - `if (a == 1 and b == 1 and c == 1) then out = 1 else out = 0`
- We can compose it from two AND gates!

![](../img/13_logical_3way-and_construction.png)

- Here we see a level of abstraction - the implementation is shown inside the dashed line, but the user never sees this
- We could use a different implementation, as long as it meets the functional spec

### Interface versus Implementation

- The 3-way AND gate above nicely demonstrates the split between interface and implementation
- The interface (what lies outside the dashed line) is all you generally care about as a user
- The interface for a given gate is unique
  - If there is more than one interface for a gate, you are probably not describing it well
  - There should only be one way to describe what a gate is supposed to do.
- The implementation for a given gate is not unique
  - There may be many implementations that all meet the same interface
  - Some may be more elegant, or use fewer internal components, or less energy, or less expensive

### Circuit implementations

- It turns out that these gates can be represented by an electronic circuit
- In these examples, the light bulb takes the role of the output
- In this course, we won't deal with physical implementation - it's Electrical Engineering (not Computer Science)

#### AND

![](../img/13_circuit_and.png)

#### OR

![](../img/13_circuit_or.png)

## 1.4 Hardware Description Language (HDL)

- HDL is a way to describe a logic gate such that we can simulate/test the gate, and finally build it

### Design: Requirements => Interface

- Lets say we want to build a XOR gate that outputs 1 if a single input is 1

![](../img/14_logical_xor.png)

- We will also need a truth table:

a | b | out
---|---|---
0|0|0
0|1|1
1|0|1
1|1|0

- This is our interface!
- In HDL (stub file):
```VHDL
/* XOR gate: out = (a AND NOT(b)) OR (NOT(a) AND b) */
CHIP Xor {
    IN a, b;
    OUT out;

    PARTS:
    /* your implementation here */
}
```
- Doing our implementation:
  - External/Interface names need to be consistent with the requirements
  - We can use whatever internal names we like to connect the internal components

```VHDL
/* XOR gate: out = (a AND NOT(b)) OR (NOT(a) AND b) */
CHIP Xor {
    // interface
    IN a, b;
    OUT out;

    // implementation
    PARTS:
    Not (in = a, out = nota); /* we can use whatever name we like for the output here because it's internal */
    Not (in = b, out = notb);
    And (a = a, b = notb, out = aAndNotb);
    And (a = nota, b = b, out = bAndNota);
    Or  (a = aAndNotb, b = bAndNota, out = out ); 
}
```

- Now we have our HDL file, a textual description of the diagram!
- Some notes on HDL:
  - HDL is a functional/declarative language
  - Order of HDL statements is insignificant
  - Customary to write them left to right, for readability purposes
  - Before using a chip, you must know its interface
  - Conventions like `partName(a=a,..,out=out)` are common as they are convenient
  - Common HDLs: VHDL and Verilog but there are many others
  - The HDL we use in this course is similar to industrial HDLs, but slimmed down for simplicity

  ## 1.5 Hardware Simulation

- In the previous section we wrote some HDL code to desctibe our gate, but there's 0 guarantee that our design is actually correct
- We still need to test that the chip delivers the interface we have specified
- To test our chip, we can use a hardware simulator in one of two modes:
  - Interactive mode, where we manually play with the chip in the simulator until we are satisfied that the chip meets our interface spec
  - Script based testing, where we define our tests beforehand in a "script file" and give the simulator the HDL file and our script file
    - We can send the output here to an "output file", and compare to a "compare file" where we define the expected output of the chip
- Test scripts will be supplied!

### Using Test Scripts
- Always specify `load $your_hdl_file` at the start, as it ensures you are testing the correct file!
- Use `output-file $your_output_file` to stick your test output somewhere, `output-list $variables` to specify the variables you want to output and then specify `output` at the end of your test lines to output your `$variables` to `$your_output_file`
- Use compare files to check that your chip is well behaved
  - Compare files have the same format as the output file
  - If we have a well-behaved chip, we can use the test output to generate a compare file for a chip of unknown behaviour
  - Chip logic can actually be implemented in a high level language!
  - This allows us to perform high level planning and testing of a hardware architecture before we get our hands dirty with HDL code

## 1.6 Multi Bit Buses

- Often we want to manipulate some array of bits
- Conceptually convenient to consider the group of bits as a single entity, sometimes called a "bus"
- HDLs usually have some convenient/special notation for managing the bus

![](../img/16_16-bit_adder.png)
- For example, we might want to add two 16-bit integers in a 16-bit adder
    - Our chip will have 32 wires as input, and 16 wires as output
- In HDL, our chip will look like this:

```VHDL
/*
 * Adds two 16-bit values together
 */
CHIP Add16 {
    IN a[16], b[16];
    OUT out[16];

    PARTS:
    /* your implementation here */
}
```
- To use our Add16:


```VHDL
/*
 * Adds three 16-bit values together
 */
CHIP Add3Way16 {
    IN first[16], second[16], third[16];
    OUT out[16];

    PARTS:
    /* temp is automatically set as a bus, because Add16's out is a 16-bit bus */
        Add16(a=first, b=second, out=temp);
        Add16(a=temp,  b=third,  out=out);
}
```

- We can also get an individual bit from a bus:
```VHDL
/*
 *  ANDs together all 4 bits of input
 */
CHIP And4Way {
    IN a[4];
    OUT out;

    PARTS:
    /* use array indexing to access individual bus elements */
        AND(a=a[0], b=a[1], out=tmp1);
        AND(a=a[2], b=a[3], out=tmp2);
        AND(a=tmp1, b=tmp2, out=out);
}
```

- Finally, we can access sub-ranges of buses
```VHDL
    ...
    IN  lsb[8], msb[8], ...;
    ...
    /* a[x..y] references bits x through y of bus a */
    Add16(a[0..7]=lsb, a[8..15]=msb, b=..., out=...);
    Add16(..., out[0..3]=t1, out[4..15]=t2);
```

- Some syntactic choices of the HDL we will use in the course:
    - Overlap of sub-buses is allowed
    - Width of internal buses is deduced automatically
    - `false` and `true` can be used as a bus of any width

## 1.7 Project 1 Overview

- Given the Nand gate, build the following:
  - Elementary logic gates:
    - Not
    - And
    - Or
    - Xor
    - **Mux**
    - **Dmux**
  - 16-bit variants
    - Not16
    - **And16**
    - Or16
    - Mux16
  - Multi-way variants
    - Or8Way
    - **Mux4Way16**
    - Mux8Way16
    - Dmux4Way
    - Dmux8Way
- Why these gates?
  - Commonly used gates
  - These are the logic gates we need to build a computer

### Elementary Logic Gates: Mux and Dmux

#### Multiplexor:
![](../img/17_mux.png)
  - `if (sel == 0) out = a else out = b`
  - 3 inputs: `a`, `b` and `sel`
  - Mux allows you to select from two inputs and output one of them
    - Widely used in digital design and communications networks

Truth table:
a | b | sel | out
---|---|---|---
0|0|0|0
0|1|0|0
1|0|0|1
1|1|0|1
0|0|1|0
0|1|1|1
1|0|1|0
1|1|1|1

Abbreviated truth table:
sel | out
---|---
0 | a
1 | b

- Uses for mux logic include using it to build a programmable logic gate
  - e.g. AndMuxOr
    `if (sel == 0) out = (a And b) else out = (a Or b)`
    ![](../img/17_and-mux-or.png)

##### Demultiplexor
![](../img/17_demux.png)
  - `if (sel==0) {a,b}={in,0} else {a,b}={0,in}`

Truth table:

in | sel | a | b
---|---|---|---
0 | 0 | 0 | 0
0 | 1 | 0 | 0
1 | 0 | 1 | 0
1 | 1 | 0 | 1

### 16-bit variants

#### And16
![](../img/17_and16.png)
- Two 16-bit buses as input
- One 16-bit bus as output
- Perform bitwise And over the two input buses
- Straightforward extension of the elementary And gate
  - Implemented with a set of two-way And gates

  ### Multi-way variants

  #### Mux4Way16
  ![](../img/17_mux4way16.png)
  - 4 inputs coming in, each is a 16-bit bus
  - 2 selection bits (we need 2 bits to select between 4 options)
  - 1 16-bit bus as output
  - Can be built from several Mux16 gates
  

## 1.8 Perspectives

Q+A Section, based on commonly asked questions in this week's worth of lectures


- We've started witht the NAND gate and worked up from there. Is it possible to start from any other point?
  - Yes! We can start with NOR (not OR), or {AND, OR, NOT}
  - This is similar to geometry, where we can start from different axiomatic systems but reach the same end result
  - NAND gates are favoured because they are often the cheapest to produce
- We started with a NAND gate and treated it as a "black box" - how would you build a NAND gate?
  - In the course we didn't do this because it's in the realm of EE/Physics - not CS.
  - One example of a physical implementation is the NMOS NAND gate
  - A and B are connected to transistors, and if A or B is negative then there is no connection from F to ground - giving us a positive signal
  - This is an older implementation - used in the 1970s, but is an example

<img src="../img/18_nmos_nand.png" width="300">

  
- How does the HDL we use in the course compare to "real" HDL languages used in industry?
  - HDLs are all about being able to design/build computers, which the HDL in the course is able to do
  - "Industrial strength" HDLs are far more complex/powerful than our HDL
    - Typically have a syntax somewhere between our HDL and C, including `for` and `while` constructs

- The chips we have built so far are simple. How do we go about building chips with dozens/hundreds of parts?
  - Complicated design challenge, many techniques, no "one size fits all"
  - "Silicon compilers" exist where you specify a desired behaviour and the compiler designs a chip, but this is not always a good process - the general problem is NP-hard
  - Use the general CS tools: abstraction/modularity