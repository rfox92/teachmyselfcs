# Lecture Set 0: Introduction

## 0.0 Introduction

- Overall course:

![](../img/00_hierarchy.png)

 - Hardware platform covered in Nand to Tetris I
 - Software platform covered in Nand to Tetris II

## 0.1 The Road Ahead

 - CS commonly maintains a strong separation between implementation and abstraction
 - We don't need to worry about the "how" (implementation), only the "what" (abstraction)
   - At some point, *someone* will need to worry about the "how" - who is it?
   - Someone else, or you at another time
 - Each week, in the course we will:
   - Worry about a single level of abstraction
   - Assume the lower level is a given/has been completely implemented correctly (we'll have done it next week)
   - Implement the higher level abstraction
   - Test that the higher level works

## 0.2 From Nand to Hack

- We start with a NAND gate. Anything lower (in the abstraction world) than this is ~~"here be dragons"~~ physics - outside of the scope of the course

### Weekly projects
- Week 1: Elementary Logic Gates:

‎  |‎  |‎  |‎   
------| ---- | ----- | ---
 Nand |  Not |   And | Or
  Xor |  Mux |  Dmux | Not16
And16 | Or16 | Mux16 | Or8Way
Mux4Way16 | Mux8Way16 | DMux4Way | DMux8Way

- Week 2: Arithmetic and Logic Unit (ALU)
  - Half adder
  - Full adder
  - Add16
  - Inc16

- Week 3: Registers and Memory
  - Bit
  - Register
  - RAM8
  - RAM64
  - RAM4K
  - RAM16K
  - PC

- Week 4: Writing low level programs

- Week 5: Computer architecture
  - Memory
  - CPU
  - Computer

- Week 6: Developing an assembler