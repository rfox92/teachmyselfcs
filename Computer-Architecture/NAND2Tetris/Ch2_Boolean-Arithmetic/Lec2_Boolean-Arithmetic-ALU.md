# Lecture 2: Boolean Arithmetic and the Arithmetic and Logic Unit (ALU)
[Coursera Link](https://www.coursera.org/learn/build-a-computer/home/week/2)

Overview: we'll use the previous module's chip set to build an *adder*, then use this to build an ALU (which acts as the center of the CPU)

Did not take notes on lec2.1

## 2.2 Binary Addition

- We want to be able to manipulate binary numbers:
  - Addition
  - Subtraction
  - Less than, Greater than
  - Multiplication
  - Division
- It turns out we only need to understand/implement addition and negative numbers, and then we get subtraction/LT/GT for free
- We can defer multiplication/division to software
  - Writing a program to divide two numbers is easier than designing hardware to do it

- How do we handle overflow?
  - We don't! Just ignore it - surely it'll be fine!
  - What we're actually doing is addition modulo word size
  - E.g. if you're considering 8-bit addition, we're really doing addition modulo 256
  - We might also have an overflow flag on our ALU outlet 

- Building an adder: 3 Steps
  - Half adder - adds two bits
  - Full adder - adds three bits
  - Adder - adds two numbers

## Lecture 2.3 Negative Numbers
 
- Finding `-x` given `x`:
  - `not(x) + 1`