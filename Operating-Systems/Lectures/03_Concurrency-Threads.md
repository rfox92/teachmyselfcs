# UC Berkeley CS 162 Operating Systems (2013)

[Playlist](https://www.youtube.com/playlist?list=PLRdybCcWDFzCag9A0h1m9QYaujD0xefgM)

## Lecture 3 - Concurrency and Thread Dispatching

##### Goals for today:

- Review Processes vs Threads
- Thread dispatching
- Co-operating threads
- Concurrency examples

### Why Processes and Threads?

Our goals are:
  - Multiprogramming: run multiple applications concurrently
    - Old idea, predates single user computers - used in mainframes that did batch processing
    - Pull several jobs in to memory to keep the CPU busy
    - Batch processing computers don't really exist any more, so multiprogramming has become a synonym for multitasking
    - Involves encapsulation of each process with its own copy/view of virtual memory
  - Protection: Don't let a rogue application interfere with other (well-behaved) applications (e.g. crashing the system, hacking the mainframe)

Solution:
  - Process: Unit of execution and allocation
    - Virtual machine abstraction: lets process believe it owns the machine
    - CPU, memory, I/O multiplexing

Next Challenge:
  - Process creation/switching expensive
  - Within a single application we might need concurrency (e.g. in a web server)

Next Solution:
  - Threads! decouple allocation and execution
  - Run multiple threads in a single process  

#### Processes vs Threads

![](../img/03_unix-process.png)

- Memory, I/O state replicated resources as a part of the process. Managed by OS
- Instruction stream is a "conventional" sequential stream of instructions (left of image)
- CPU state maintained by OS
  - Stored (to RAM) on context switch out of a given process
  - Restored (from RAM) on context switch back in to a given process

##### Processes
![](../img/03_process-sched.png)

- Switch overhead (between processes) is high
  - CPU state is low usage
  - Memory/IO in high usage
- Process creation high overhead
- Protection provided for CPU and Memory/IO
- Resource sharing overhead: high
  - Involves at least a context switch


##### Threads
![](../img/03_thread-sched.png)

- Switch overhead (between threads) is low
  - Only CPU state is restored
  - Registers initialised, stack space allocated/initialised
- Thread creation lower overhead
- Protection *only provided for CPU* - memory and IO are unprotected
- Resource sharing overhead: low
   - Thread switch overhead is low

##### Multicore Systems
![](../img/03_multicore-sched.png)

- Switch overhead (between threads) is low
  - Only CPU state is restored
  - Registers initialised, stack space allocated/initialised
- Thread creation lower overhead
- Protection *only provided for CPU* - memory and IO are unprotected
- Resource sharing overhead: low
   - Thread switch overhead is low

##### Hyperthreaded Systems

![](../img/03_hyper-sched.png)

- Each core may have multiple copies of registers
- Switch overhead is much lower than previous cases - often high level of hardware support so single instruction
- ALUs/FPUs [^1] not duplicated within a core so contention there may hurt performance

[^1]: Arithmetic and Logic Unit (ALU), Floating-point unit (FPU)

##### Process Control Block

```C
PCB = 
{
    process_state,
    process_number,
    program_counter,
    registers[],
    memory_limits,
    open_files[],
    ...
}
```

PCB is required to manage a multithreaded system - it contains the information needed by the OS to save/restore a process' state.
Multithreaded programs use a Process Environment Block (PEB), and thread control is managed by a Thread Control Block (TCB)

##### Context Switching

![](../img/02_context-switch.png)

- Code executed in the kernel above is overhead
  - Code overhead sets minimum practical switching time
  - Less overhead with Hyperthreading but you end up with contention for resources instead

Code executed in the context of P<sub>0</sub> or P<sub>1</sub> is running in what is called "user mode", code executed in the context of the operating system is running in what is called "kernel mode". 
Kernel mode is an expanded security context where you can access additional resources than user code (e.g. can see other process' address spaces)

##### The Numbers (as of 2013)

- Context switching in Linux: 3-4 usecs (2013 Kernel, i7)
- Thread switching only slightly faster than process switching (~100ns/0.1us) on Linux
- Modern CPUs have dedicated hardware for speeding up context switching but modern OSes (in 2013) are not using this feature
- PCB is small, so it can sit inside the L1 cache on the CPU
  - This means everythign required for a restore is sitting very close to the CPU (in a cache) rather than in memory as our abstraction would make us think
- Switching across cores is about twice as expensive as switching within a core
  - Switching across cores can't use the L1/L2 cache as this is restricted to a single core
  - Only the L3 cache is shared between cores
- Context switching is very sensitive to the sive of the working set [^2]
  - When we switch out of a process, the working set of the process slowly gets evicted from the cache
  - When we switch back in to the process we need to rebuild the cache
- ***Overall, context switch speed depends mostly on cache limits and process/thread memory usage***
- Caveat: many processes are multithreaded
  - "Typical" context switch may involve many thread context switches
  - 1-2 orders of magnitude slower than a single thread context switch

[^2]: Working set is the memory used by a process "recently" - set of memory locations it has hit recently/frequently

##### Thread State
- Some state shared by all threads in a process/address space
  - Memory content (global variables, heap)
  - I/O state (file system, network, etc)
- Some state private to each thread
  - State stored in Thread Control Block (TCB)
  - CPU registers
  - Execution stack
    - Local parameters, temporary variables
    - Return program counters kept while procedures execute
    - Permits recursive execution
    - Crucial to modern languages

### Thread dispatching

```C
main()
{
    ComputePI("pi.txt");
    printf("hello, world!\n");
}
```

- Issue with the above is that it will never print "hello world!" - ComputePI should never return

```C
main()
{
    CreateThread(ComputePI("pi.txt"));
    CreateThread(printf("hello, world!\n"));
}
```
- In our improved version:
  1. CreateThread starts a thread, then returns
    1.1.Thread computes PI
  2. CreateThread starts a thread, then returns
   2.1. Thread prints "Hello, World"
- Calculations run independently
- *Should* behave as if we have two separate CPUs executing code simultaneously
- If we stop the program and break out `gdb`, what do we see?
  - Two sets of CPU registers
  - Two stacks

![](../img/03_multithread-address.png)

- Problems:
  - How do we position the stacks relative to each other?
  - What should our maximum stack size be?
  - What do we do if a thread violate this?
  - How do we detect violations?

#### Thread State
- Each thread has a Thread Control Block (TCB)
```C
TCB = 
{
    registers[],
    program_counter,
    stack_pointer,
    scheduling_state,
    scheduling_priority,
    scheduling_cpu_time,
    *enclosing_pcb
}
```
In addition, we have some pointers for implementing scheduling queues.
OS keeps TCBs in protected memory.

##### Thread/Process Lifecycle

![](../img/02_process-lifecycle.png)

Thread/Process state over lifetime
- `new`: thread in creation process
- `ready`: thread waiting to be run
- `running`: instructions being executed
- `waiting`: waiting on some event (e.g. I/O)
- `terminated`: thread finished execution





##### Ready Queue

- At any one time, we will have many more `ready` threads than `running` threads
- If the thread is not `running` then it is in some scheduler queue
  - Separate queue for each device/signal/condition
  - Each queue can have a different scheduler policy

![](../img/03_sched-queues.png)

#### Thread Dispatch Loop

- Conceptually, OS dispatch loop looks like this:

```C
Loop {
  RunThread();
  ChooseNextThread();
  SaveThreadControlBlock(curTCB);
  LoadThreadControlBlock(newTCB);
}
```

- This is the main function of the OS.

#### `RunThread()`
- How to run thread?
  1. Load thread state (registers, stack pointer) to CPU
  2. Load environment (virtual memory mapping/space)
  3. Jump to program counter (PC) of thread
- How to get control back from the thread?
  - Internal events: thread voluntarily returns control to OS
  - External events: OS jumps in and takes control

##### Yielding through Internal Events
- Implicit yields caused by:
  - Blocking on I/O
  - Waiting on signal[^3] from other thread
- Explicit `yield` syscall, where thread volunteers to give up CPU
[^3]:Signalling is a low level mechanism for inter-thread communications

Early Windows (3.1) had multitasking implemented by adding many `yield` calls throughout e.g. spreadsheet code.

User code needs to be isolated from kernel code, so we usually are running two independent stacks: one for the userland and one for the kernelland. The `yield` call causes a context switch in to the OS.
We want to isolate user/kernel code because the kernel is highly privileged - we don't want badly written user code to be able to interfere with the kernel.

#### Interrupt Controller

![](../img/03_interrupt-controller.png)

- Interrupts invoked with interrupt lines from devices
  - I/O writes complete
  - Mouse/keyboard events
- Interrupt controller choses which interrupt request to honour
  - Interrupt Mask enables/disables interrupts
  - Priority Encoder chooses the highest enabled interrupt
  - Software interrupt set/cleared by software
  - Interrupt identity specified with interrupt ID line
- CPU can disable all external interrupts with internal flag
- Non-maskable interrupt (NMI) line cannot be disabled
- Interrupts can be readily translated in to a system call

#### Preemptive multithreading

- Preemptively multithreaded OSes use the timer interrupt to regularly wrest control from threads 
- Our timer interrupt routine looks something like this:

```C
TimerInterrupt()
{
    DoPeriodicHouseKeeping();
    RunNewThread();
}
```
- Called "*preemptive multithreading*" because we don't rely on the kindness of threads to get control
- Manually calling `yield` is still a strategy in real-time systems, as it means you are less likely to receive a timer interrupt at an inappropriate time

#### Why allow co-operating threads?

1. People co-operate 
2. Computers exist to help/enhance people's lives
3. Computers must co-operate

###### Advantage 1: Resource Sharing
- One computer, many users
- One bank account, many ATMs
  - What if ATMs only updated at night?
- Embedded systems (robot control: co-ordinate arm and hand)

###### Advantage 2: Speedup
- Overlap I/O and computation
- Multiprocessors - chop up program in to parallel pieces

###### Advantage 3: Modularity
- Chop up program in to simpler pieces
- Makes systems easier to extend

##### Multithreaded Web Server
```C
serverLoop()
{
    connection = AcceptConnection();
    ThreadCreate(ServiceWebPage(), connection);
}
```

- Advantages of threaded version:
  - Can share cached files kept in memory
  - Threads cheaper to create than processes, so we have lower per-request overhead

##### Thread Pools
- Problem with previous web server example: thread count is unbounded
  - When multiple requests come at the same time, throughput sinks as we have to context switch more and more
- Instead, we want to allocate a bounded "pool" of threads, representing the most we want to multithread
- Use one `master` thread that manages multiple `slave` threads

```C
master()
{
    allocateThreads(slave, queue);
    while(true)
    {
        connection=acceptConnection();
        Enqueue(queue, connection);
        wakeUp(queue);
    }
}
slave(queue)
{
    while(true)
    {
        if (null == (connection = Dequeue(queue)));
        {
            sleepOn(queue);
        }
        else
        {
            ServiceWebPage(connection);
        }
        
    }
}

```
