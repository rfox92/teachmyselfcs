# UC Berkeley CS 162 Operating Systems (2013)

[Playlist](https://www.youtube.com/playlist?list=PLRdybCcWDFzCag9A0h1m9QYaujD0xefgM)

## Lecture 1 - Overview

### Course goals
  - Learn how systems work
  - Main challenges in building systems
  - Principles of system design
  - Application of the principles to building systems

### Systems Challenges

#### Scale
  - We have large scale, heterogeneity and dynamic range
    - CPUs: Individual sensors => GPUs
      - Cores: 1-100s cores (2 order of magnitude variation)
      - Clusters: Few machines - 10,000 machines (4 order of magnitude variation)
    - Network: Inter-core network (inside CPU) => Internet
      - Latency: ns-s (9 order of magnitude variation)
      - Bandwidth: Kbps-Gbps (6 order of magnitude variation)
    - Storage: Caches => Disks
      - Size: MB-TB (6 order of magnitude variation)
      - Access time: ns-ms (6 order of magnitude variation)

We need ways for these wildly varying systems to interact in a robust manner.

Compare the range in computing devices with the range in automotive devices:
    - Engine power: 50HP-1000HP (20x range)
    - Speed: 100km/h-400km/h (4x range)
    - Weight: 500kg-20,000kg (40x range)

The ranges are much smaller here, cars/trucks are much less diverse than computing devices.

#### Complexity
Applications that are made up of:
- Variety of software
- On a variety of devices/machines
  - That may implement different hardware architecture
  - Running competing applications
  - Can be under a variety of attacks
  - That may fail in unexpected ways

This means it's not feasible to test software for all possible environments/component configuration. **The question is not *"are there bugs"*, but *"how serious are the bugs"***. This means it's important to use a conservative design philosophy, as it is more likely to lead to a safe system.

#### Computer System Organization

- One or more CPUs, Device Controllers connect through a common bus
- All access shared memory - most devices are memory mapped to some level


#### Example: Pathfinder Mars Rover Requirements (1996)

- NASA limitations on hardware complexity:
  - 20MHz processor, 128MB of DRAM, VxWorks OS
  - Cameras, scientific instruments, batteries, wheels
  - Many independent processes work together
  - Limitations are characteristic of NASA/Military requirements - very conservative as all system components need to be validated
  - VxWorks - RTOS
    - RTOS means we are largely interrupt driven, lots of timer interrupts
    - Designed to respond quickly to most device events
    - Lots of failsafe features - if a process blocks a processor for too long, the OS reboots
    - Must always be able to receive commands
  - Individual programs cannot be allowed to bring down the OS
    - The network stack should not be able to crash the antenna positioning software
  - All software *may* crash occasionally
    - Auto restart with diagnostics sent back to Earth
    - Periodic checkpointing of state?
  - Some functions time critical
    - Need to deploy parachute before we hit the surface too hard
    - Must track earth orbit for communications

#### Taming Complexity

- All computer hardware is different
  - Different CPUs
    - x86, x64, ARM, PowerPC, ColdFire
  - Different amounts of memory, disk
  - Different Devices
    - Mice, keyboards, sensors, cameras,...
  - Different networking
    - Fibre, Wireless, Satellite

Should we write one program with all of our functions, or many smaller functions?

|Monolithic Pros | Monolithic Cons |
|---|---|
|Networking/IPC is easier | Crashes bring down entire system |
| Easier to model | **Complexity explodes** |
| Speed | |

Many RTOS are based on microkernel architecture for complexity management.

Should our software be hardware aware?

| Hardware awareness Pros | Hardware awareness Cons | 
| --- | --- | 
| Can fully exploit the hardware | Less flexibility |
| Can communicate/compute as fast as possible | Tied to single device/architecture |
| | More difficult to upgrade as hardware changes |


#### Virtual Machines
  - Software emulation of abstract machine
    - One machine "putting on the skin of another"
  - Gives programs running on VM illusion that they own the machine
  - Makes it appear that the hardware has features you care about

Two types of VMs:
  - System Virtual Machine
    - Supports execution of an entire OS and applications
    - Xen/ESXI/Hyper-V
  - Process Virtual Machine
    - Supports execution of a single program
    - Functionality typically provided by OS 
    - JVM/.NET Framework

##### Process VMs
- Each process thinks it has
  - All the memory, CPU time
  - Ownership of all the devices
  - Different devices seem to have the same interface
  - Device interfaces more abstract than the underlying hardware
    - Bitmapped display is turned in to a windowing system
    - Ethernet card is turned in to a source of reliable, ordered networking
- Isolation allows us to narrow down faults
  - Processes are unable to directly impact other processes
  - Bugs cannot crash whole machine
- Protection and portability
  - Java/.NET stable across platforms

##### System VMs
- Useful for OS development
- When OS crashes, only one VM crashes
- Allows for testing program on another OS

#### What does an Operating System Do?

- "An OS is similar to a government" (Silberschatz and Gavin)
  - Does a government do anything useful by itself?

- Operating Systems as government:
  - Manage system resources
  - Settle conflicting requests for resources
  - Prevent errors, improper use of computer

- Operating Systems as facilitator:
  - **Provide useful abstractions**
  - Standardise libraries, windowing systems
  - Make application programming easier, faster, less error-prone

- Some features reflected in both tasks
  - File system is needed by everyone (facilitator)
  - File system must be shared and protected (government)

#### What is an OS then?

- Usually performs:
  - Memory management
  - I/O Management
  - CPU Scheduling
  - Synchronization
  - Communications
  - Multitasking

- Also maybe manages...
  - File System
  - Multimedia support
  - UI

- No universally accepted definition
- "Everything the vendor ships you when you order an OS" is an ok approximation, but varies a lot
- The one program running at all times on the computer is the **kernel**. 
    - The heart of the OS.
- Everything else is either a system program (ships with the OS) or an external application

#### Summary

 - OS provides VM abstraction to handle diverse hardware
 - Co-ordinates resources, protect users/applications from each other
 - Simplify application development by standardising abstractions
 - Can provide fault tolerance/containment/recovery