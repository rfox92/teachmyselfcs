# UC Berkeley CS 162 Operating Systems (2013)

[Playlist](https://www.youtube.com/playlist?list=PLRdybCcWDFzCag9A0h1m9QYaujD0xefgM)

## Lecture 2 - Concurrency: Processes, Threads, Address Spaces

### Operating System Roles (Recap)
- OS as a government
   - Manages resources
   - Settles conflicting requests for resources
   - Prevents errors, improper system use
   - Nudges users towards standard interfaces, improving sharing
     - E.g. stdin/stdout, pipes on Unix
- OS as a facilitator
   - Provides useful abstractions
   - Standardise libraries, windowing system
   - Provides facilities/services needed by everyone
   - Eases application programming, reduces error surface

### OS Archaeology

- OSes take a lot of time/effort (100s-1000s of person-years) to develop
- Most modern OSes have a long lineage
  - Multics -> AT&T Unix -> BSD Unix -> Ultrux, SunOS, NetBSD
    - Many concepts present in Multics endure to this day, even though Multics was developed in the 1960s
    - AT&T Unix was intended to fix the issues present in Multics
    - BSD Unix an improvement on AT&T Unix
  - Mach (micro-kernel) + BSD -> NextStep -> XNU -> Apple OSX, iOS
    - Microkernel: kernel that just meets the bare minimum OS functions in kernel mode
    - Typically just includes threading, memory management, basic inter-process communications (IPC)
    - File system, device drivers sit outside the kernel but still interact with the OS
  - Linux -> Android OS
  - CP/M -> QDOS -> MS-DOS -> Windows 3.1 -> NT -> 95 -> 98 -> 2000 -> XP -> ...
    - NT was a significant rewrite, almost microkernel
  - Linux -> Arch, Fedora, Debian, Suse, ... 

#### Goal for this lecture:

  - How do we provide multiprogramming?
  - What are processes?
  - How do they relate to threads and address spaces?

### Threads
- Unit of execution
  - Independent Fetch/Decode/Execute loop
  - Unit of scheduling
  - Operating in some **address space**
    - Has its own stack+registers but otherwise can share memory with other threads
    - Unlike a process, it does not own its address space
  - Contains enough information such that you can save/restore it

- Review: Fetch/Decode/Execute loop
0. Start of loop: Program Counter (PC) pointing at an address with some code
1. Fetch Instruction at PC
2. Decode Instruction
3. Execute Instruction (possibly using registers)
4. Write results to registers/memory as appropriate
5. Update PC - either increment or jump
6. Repeat

**Uniprogramming**
  - One thread at a time - batch processing
  - Easier to develop operating system
  - Don't need to manage concurrency
  - Bad for modern PCs with GUIs: responsive GUIs require additional threads

**Multiprogramming**
  - Multiple threads at a time
  - Often called "multitasking" but this is an imprecise definition

### Multiprogramming
- Each application wants to "own" the machine - you need to provide a VM abstraction
- Applications compete for resources
  - Need to manage access to shared resources -> **concurrency**
  - Need to isolate applications from each other -> **protection**
- Applications need to communicate/co-operate with each other -> **concurrency**
- Leads to increased utilization of the CPU
  - E.g. when a thread needs to write to disk, the CPU can issue a Direct Memory Access (DMA) command to the disk, so that the disk performs the write from memory itself. After issuing the DMA command, the CPU can move to the next task *while the disk executes the write*

### Processes
- Unit of resource allocation *and* execution
  - Owns memory/address space
  - Owns file descriptors, file system context, ...
  - Encapsulates 1+ threads sharing process resources
- In contrast to the thread, which is only a unit of execution
- Why processes?
  - Helps navigate tradeoff between protection and efficiency
  - Provide memory protection
    - Threads share memory, so do not provide memory protection
  - Threads are more efficient than processes
    - In a typical OS, thread creation is ~30x faster than process creation
    - Context switching between processes is about 5x more expensive than context switching between threads
- Application instances consist of one or more processes

### Concurrency

- The basic Concurrency problem is one of resouce management:
  - Hardware: single CPU, single DRAM, single I/O devices
  - Multiprogramming API: processes want to think they have exclusive access to shared resources
- OS needs to juggle.
  - Managing the mapping from the virtual interfaces (provided by the multiprogramming API) to the underlying physical device
    - E.g. the programmer uses some abstraction such as `open()` to open a file, and the OS will look and say something like "well, there's another process using that file right now"
    - Depending on the specifics of the `open()` call, the OS will either: 
      - Block the second `open()` until the first `open()` finishes (possibly via a `close()`)
      - Return some error to the program if the programmer has uses a "non-blocking call" to allow the program to do something else
    - Non-blocking calls in a sense can see through the abstraction a little - all abstractions are leaky to some degree
    - These problems don't have simple answers, hence the existence of blocking/non-blocking calls, so that the programmer can do the most appropriate thing in that context
  - We have multiple processes, I/O interrupts
  - How can the OS keep all the balls in the air?
- Basic idea: use a Virtual Machine abstraction
  - Simple machine abstraction for processes
  - Multiplex these abstract machines

#### The Illusion of Multiple Processors

Assume we have a single processor (i.e. single CPU core). How do we provide the illusion to all the processes on the system that they each "own" a processor?
  - Multiplex in time! Time slicing!

![Time Slicing](../img/02_timeslice.png)

- pCPU is going to execute small chunks of code for each of the vCPUs in sequence
- pCPU will be "busy" all of (or most of) the time
- Slices must be narrow enough such that the processes can pretend that they are the sole owner of the physical hardware
- Each vCPU needs a structure to hold:
  - Program Counter (PC)
  - Stack Pointer (SP)
  - Registers

- Call this a "State Block" or "Program Control Block" (PCB)
- How do we switch from one vCPU to the next?
  - Save PC, SP, registers to current state block
  - Load PC, SP, registers from new state block
  - Since these units of execution are sharing memory, they are threads (not processes)
  - When we save state, we only need to worry about our SP, PC and registers. If we were switching between processes, we would also need to change our address space around.

- When do we context switch?
  - Timer - means the OS is pre-emptive. 
  - Voluntary yield - process says "I don't have anything to do right now so I'll not waste CPU time"
    - `yield()` is typically the system call to achieve this
  - I/O

###### Properties of this simple multiprogramming technique

- All vCPUs share the same non-CPU resources
  - I/O devices are the same
  - Memory is the same
- Consuequence of sharing
  - Each thread can access the data of each other thread - good for sharing, bad for protection
    - Enables more powerful/faster inter-thread communication
  - Threads can share instructions - good for sharing, bad for protection
  - Can threads overwrite OS functions?
    - No! How do we prevent that from happening?
    - Need to protect OS code from being written to
  - This model is seen in:
    - Embedded applications
    - Windows 3.1/Early Macintosh (switch only with yield)
    - Windows 95 - ME (switch with both yield and timer)

#### Modern technique: Simultaneous Multi/Hyper Threading

- Hardware-enabled technique
- Uses the properties of modern processors to provide illusion of multiple processors
- Need to replicate registers, but overall higher utilization of resources

![Sorry for blurry image](../img/02_hyperthreading.png)

Image above:
  - First column shows naive timeslicing implemented on single core system
  - Middle column shows additional thread in green, CPU is processing 
  - Right column shows hyperthreading architecture
  - Core might have issued memory fetch operation, which is slow from the POV of the CPU
    - While that fetch is occurring, it can switch to another thread to execute some arithmetic operation that was prepared earlier
    - Results in the CPU being busy for more time
  - Hyper-threading can give you speed improvement, but not always
    - If you are memory-intensive, you end up switching between threads often
    - Can lead to more cache misses or thrashing
  - Can schedule each thread as if it were a separate CPU
    - Non-guaranteed, non-linear speedup
- Original implementation was called ["Simultaneous Multithreading"](https://dada.cs.washington.edu/smt/index.html), developed in mid-1990s
- Does result in a power increase, making it somewhat inappropriate for mobile devices

##### How do we protect threads from each other?

- Protection of memory
  - Each thread does not have full access to memory
- Protection of I/O devices
  - Threads do not have access to all I/O devices
- Protection of pCPU
  - Pre-emptive/time based context switching
  - Must not be able to disable timer from userland

###### Recall: Address Space
- Address Space: set of accessible addresses and associated states
  - For a 32-bit processor, there are 2^32 addresses (~4 billion)
  - `stack` grows from higher addresses to lower addresses
  - `heap` grows from lower addresses to higher addresses
  - `text` contains program code
  - `data` contains fixed size data
![](../img/02_address-space.png)

- What happens when you read/write to an address?
  - Nothing?
  - Acts like regular memory?
  - Ignores write?
  - Causes I/O operation? (in the case of memory-mapped I/O)
  - Causes exception/fault?

- In order to be able to implement processes, we need to introduce a layer of indirection
- Processes need to have their own (isolated) view of the address space


![](../img/02_memory-translation-map.png)

- Requires particularly careful management of heap and stack areas
- Two prcesses above see their own Virtual Machine, but the physical machine can be seen to be shared (image center)
- Process thinks its address space is contiguous (as in our idealized version of an address space), but it is often mapped to physical memory non-contiguously

#### Traditional UNIX Process

- Process is an OS abstraction to represent what is required to run a single program
  - Often called a `HeavyWeight Process` - heavier abstraction
  - Formally: single, sequential stream of execution in its own address space
- Two components:
  - Sequential program execution stream
    - Code executed as a single, sequential stream of execution (thread)
    - Includes state of PC, SP, registers
  - Protected resources
    - Main memory state
    - I/O state (e.g. file descriptors)
- **No concurrency provided by a heavyweight process**

- Current state of a process is held in a Process Control Block (PCB)
  - "Snapshot" of execution/protection environment
  - Only one PCB active at a time (in simplest picture)

```C
PCB = {
  process_state,
  process_number,
  program_counter,
  registers[],
  memory_limits, // mapping of virtual -> physical
  open_files[],
  ...
}
```

- OS gives out CPU time to different processes (*scheduling*)
  - Only one process running at one time
  - Give more time to more important processes
- Gives pieces of resources to different processes (*protection*)
  - Controlled access to non-CPU resources
  - Examples:
    - Memory mapping: each process has its own address space
    - Kernel/User Mode: Arbitary multiplexing of I/O through system calls
    - May have access controls on the I/O objects themselves

#### Context Switch

![](../img/02_context-switch.png)

- When a CPU switches from one process to another
- Code executed in Kernel above is overhead
  - Overhead is the minimum practical switching time
  - Less overhead than hyperthreading
  - Contention for resources instead

#### Process Lifecycle

![](../img/02_process-lifecycle.png)

- `ready` means we have everything that we need to run, but is not running yet
- OS keeps a queue of `ready` processes, and based on resource availability and process priority, processes move to the `running` state
- Stay in `running` until either an interrupt or we have to `wait` on something
  - Then we move back to `ready`, not `running` - the OS should be in charge of what is `running`
- Eventually the process finishes execution and is `terminated`

#### Process =? Program



Program|Process
---|---
<code>int main()<br>{<br>...;<br>}<br>void a()<br>{<br>...;<br>}</code>|<table><td><code>int main()<br>{<br>...;<br>}<br>void a()<br>{<br>...;<br>}</code></td><td>Heap<br>Stack<br>a()<br>main()</td></table>

- A process is more than just a program
  - Program is just one part of the process state
  - `vim 02_Concurrency.md` is a diffferent process to `vim 01_Overview.md` but the same program (`vim`)
- A process is less than just a program
  - A program may invoke more than one process
  - `cc` invokes `cpp`, `cc1`, `cc2`, `as` and `ld`

#### Processes Collaborate on Tasks

- Need some sort of inter-process communication (IPC)
  - Isolating the address spaces isolates the processes (duh, it's the whole point)
  - We can do shared memory mapping
    - Map subset of internal address space of multiple processes to common DRAM
    - Read and write through memory
  - Message passing
    - `send()` and `receive()` messages
    - Works across network


##### Shared Memory Communication

![](../img/02_shared-memory.png)

- Communication occurs by *"simply"* reading/writing to shared address page
  - Low overhead communication
  - *Is not actually simple*
  - Introduces complex synchronization problems
  - Each application program needs to work out:
    - When memory writes are complete
    - Who has access to which shared memory

#### Inter-Process Communication (IPC)

- Mechanism for both *communication* and *synchronization*
- Message system - processes communicate with each other without resorting to shared variables
- More common usage: easier to set up and usually less error prone
- Canonically IPC provides two operations:
  - `send(message)`
  - `receive(message)`
- If processes P and Q want to communicate, they need to
  - Establish a communications channel between them
    - Sometimes use `sockets` - elegant solution that supports both local and remote communication
  - Exchange messages via `send()` and `receive()` primitives
- Communication link can be implemented:
  - Physically (e.g. shared memory, hardware bus, syscall, trap)
  - Logically (e.g. logical properties)

#### Modern "Lightweight" Process with Threads
- Thread: *sequential execution stream within a process*
  - Process still contains a single address space
  - No inherent protection between threads
- Multithreading: *single program made up of a number of different concurrent activities*
  - Sometimes called *multitasking*

#### Single vs Multi Threaded Processes

![](../img/02_single-multi-thread.png)

- Threads share code, data, files
- Do not share registers or stack - means they can run independently of each other
- Encapsulate concurrency

##### Examples of multithreaded programs

- Embedded systems
- Modern OS kernels
  - Internally concurrent as they are dealing with concurrent requests with multiple users
  - No protection needed within the kernel
- Database servers
  - Important example of complexity and concurrency being hidden from users
  - Databases provide a macroscopic abstraction of shared memory (in a way)
  - Manage this chunk of quasi shared memory safely
  - Access to shared data by many concurrent users
  - Background processing also required


#### Operating System Classification

- Operating systems either have:
  - One or many address spaces
  - One or many threads per address space

![](../img/02_classification.png)


#### Summary

- Processes provide two tools:
  - Threads (concurrency)
  - Address Spaces (protection)
- Concurrency accomplished by multiplexing CPU time (context switch)
  1. Unload current thread (PC, registers)
  2. Load new thread (PC, registers)
  - This context switching may be:
    - Voluntary (`yield()` syscall)
    - Triggered by I/O
    - Timer based
- Protection accomplished restricting access
  - Memory mapping isolates processes from one another
  - Dual mode from isolating I/O and other resources