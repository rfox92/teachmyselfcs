# UC Berkeley CS 162 Operating Systems (2013)

[Playlist](https://www.youtube.com/playlist?list=PLRdybCcWDFzCag9A0h1m9QYaujD0xefgM)

## Lecture 04 - Synchronization, Atomic operations, Locks


#### Challenge of Concurrency

- In an implementation of ATM software, our `Deposit()` call might look like this:
```C
Deposit(acctID, amount){
    acct = GetAccount(acctID);  // May use disk??
    acct->balance += amount;
    StoreAccount(acctID);       // Probably uses disk
}
```

- Shared state can be corrupted if we context switch
- Could be caused by load/store account->balance (esp. if it involves disk I/O)

```
//thread1                       //thread2
load r1, account->balance
                                load r1, account->balance
                                add r1, amount2
                                store r1, accout->balance
add r1, amount1
store r1, account->balance
```

- If this occurs, only the amount1 is added to the balance

#### Today's Goals

- Concurrency Examples
- Synchronization
- Hardware support

#### Correctness Requirements

- Threaded programs must work for all interleavings of the threaded instruction sequences
  - (They must provide a consistent/correct result, even if the OS scheduler parks the thread at an arbirtary point)
- Co-operating threads are inherently non-deterministic, making them much harder to reproduce

#### Atomicity

- An atomic operation is an operation that either:
  - Runs fully to completion
  - Does not run at all
- Atomic operations are indivisible - they cannot be stopped part way through
- Prevents unexpected modification of state
- They are fundamental building blocks of concurrent programs: if there are no atomic operations, there is no way for threads to co-operate

- On most machines, memory references and assignments (of 32-bit words) are atomic
- Many instructions are not atomic
  - Double precision floating point store is often not atomic
  - Some architectures have a non-atomic operation to copy a whole array
    - Single instruction, non-deterministic! Introduce concurrency bugs with raw assembly!

#### Concurrency Challenges
- Multiple threads executing in parallel to share resources and/or data
- Fine-grained sharing: more complex, but as soon as we increase the level of concurrency we see better performance
- Coarse-grained sharing: easier to implement but a lower performance ceiling w.r.t. concurrency

##### Definitions
- `Synchronization`: using atomic operations to ensure threads co-operate safely
  - For now, only loads/stores are atomic
  - It's hard to build anything useful with just reads/writes
- `Critical Section`: piece of code that only one thread can execute at once
  - All other threads must wait to "enter" the critical section if a thread is already executing/inside the critical section
- `Mutual Exclusion` (aka mutex): way to ensure that only one thread enters a critical section at once
  - One thread excludes the other while it is inside the critical section
  - Critical section/mutex are synonyms
- `Lock`: construct that prevents someone from doing something
  - Lock before entering critical section or accessing shared data
  - Unlock when leaving critical section or after accessing shared data
- `Wait`: if you cannot access a shared resource, you should wait until you can
  - All synchronization involves waiting

#### Synchronization Solution 1

- Only reads and writes are atomic

```C
void atomic_increment(int *var)
{
    if (unlocked(var))
    {
        lock(var);
        var += 1;
        unlock(var);
    }
}
```

- This doesn't always work - we can be context switched after checking if the var is locked but before applying the lock

#### Synchronization Solution 2

- Use two locks? `lockA` and `lockB`

```C
void atomic_incrementA(int *var)
{
    lockA(var);
    if (unlockedB(var))
    {
        var += 1;
    }
    unlockA(var);
}
```

```C
void atomic_incrementB(int *var)
{
    lockB(var);
    if (unlockedA(var))
    {
        var += 1;
    }
    unlockB(var);
}
```

- Still could skip our increment if we context switch immediately after applying the A or B lock!
- Called *starvation*

#### Synchronization Solution 3

```C
void atomic_incrementA(int *var)
{
    lockA(var);
    while(lockedB(var)) { wait(); }
    var += 1;
    unlockA(var);
}
```

```C
void atomic_incrementB(int *var)
{
    lockB(var);
    if (unlockedA(var))
    {
        var += 1;
    }
    unlockB(var);
}
```

- This is the first one that works!
- Both guarantee that we increment exactly once
- Either: it's safe to buy, or the other thread will buy and it is safe to quit

##### Discussion of the Solution
- Solution protects a single critical section piece of code for the thread
- That being said, it's pretty complex
- ThreadA has different code to threadB. How would this work for >2 threads?
  - Code needs to be slightly different for each thread
- While threadA is waiting, it's consuming CPU time
  - This is `busy waiting` and is a bad idea/use of CPU resources


There must be a better way! What if the hardware gives us a better primitive than atomic load/store?

#### Hardware Supported Lock Primitives
- If we have some magical implementation of a lock:
  - `Lock.Acquire()` - waits until the lock is free, then grabs it
  - `Lock.Release()` - unlock, wake up something waiting on lock
  - Operations are atomic: if two threads are waiting on a lock, only one succeeds in grabbing the lock
- We need a hardware lock instruction
  - Is it a good idea??
  - What about sleeping a thread?
    - How do we handle the interaction between hardware and scheduler?
    - May end up being OS dependent?
  - Complexity?
    - Adding instructions complicates the hardware and can slow it down

#### What about disabling the timer interrupt?

- The thread dispatcher gets control in two ways:
  - Internal: thread does something to hand control to the thread dispatcher (e.g. I/O, network, yield)
  - External: interrupt causes thread dispatcher to get CPU control

- This leads to a naive implementation:

```C
void acquire_lock() { disable_interrupts(); }
void release_lock() { enable_interrupts(); }
```

- This is really bad. What if:

```C
acquire_lock();
while (true) {;}
```
- It will hang the system!
- Even without the malicious example above, if we have a real-time system it removes any guarantees on timing
  - Critical sections might be arbitrarily long

#### Disabling timer interrupts...but better

- What about maintaining a lock variable and imposing mutex only on operations on the lock?

```C++
bool locked = false;

void Acquire()
{
    disable_interrupts();
    if (true == locked)
    {
        enable_interrupts();
        yield(); //puts thread on wait queue
    }
    else
    {
        locked = true;
    }
    enable_interrupts();
}

void Release()
{
    disable_interrupts();
    locked = false;
    enable_interrupts();
}
```

#### Summary

- Atomic Operations!
  - Operations that run fully to completion or not at all
  - Required to construct other synchronization primitives
- Constructing a Lock via interrupt enable/disabling
  - Need to be very careful here
  - Easy to tie up machine resources if we are not careful
  - Crux: Use a separate lock variable, use hardware mechanisms to protect the variable

