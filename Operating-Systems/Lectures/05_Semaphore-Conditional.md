# UC Berkeley CS 162 Operating Systems (2013)

[Playlist](https://www.youtube.com/playlist?list=PLRdybCcWDFzCag9A0h1m9QYaujD0xefgM)
## Lecture 5 - Semaphores and Conditional Variables

### Atomic Read-Modify-Write instructions

- Problems with interrupt based locks:
  - Can't give implementation to users - the implementer needs to be trusted by the OS!
  - Doesn't work on multiprocessor systems
    - Only stops thread switching on current core
    - Threads on alternate cores will not "respect"/be aware of the lock!
- Alternative: Atomic instruction sequences
  - Instructions that read a value from memory, write a new value atomically
  - Responsibility passed to hardware
    - Uniprocessors (moderately hard, works on x86 but only partially on MIPS)
    - Multiprocessor implementation needs help from cache coherence protocol
      - Changes need to be propagated around all of the caches
  - But it works on both { uni | multi } - processors!

