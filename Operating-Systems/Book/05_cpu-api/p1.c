#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
  printf("hello world (pid:%d)\n", (int) getpid());
  int rc = fork(); //call the fork system call, put the return code in rc
  if (rc < 0)
  {
    fprintf(stderr, "fork failed, exiting\n");
    exit(1);
  }
  else if (rc==0)
  { // child process gets rc of 0
    printf("hello, I am child (pid:%d)\n", (int) getpid());
  }
  else
  { // parent process gets rc of the child process pid
    printf("hello, I am parent of %d (pid:%d)\n", rc, (int) getpid());
  }
  return 0;
}
