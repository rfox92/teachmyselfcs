#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

void sigHandler(int signum)
{
    printf("  parent pid:%d\n", (int) getpid());
    exit(0);
}


int main (int argc, char *argv[])
{
  int x = 100;
  int parentPid = getpid();
  printf("hello, I am parent (pid:%d)\n", parentPid);

  int fork_rc = fork();
  if ( 0 > fork_rc )
  {
    fprintf(stderr, "fork failed\n");
    exit(1);
  }
  else if ( 0 == fork_rc )
  { //child process
    printf("  child pid:%d\n", (int) getpid());
    //send SIGCONT to parent
    kill(parentPid, SIGCONT);
    exit(0);
  }
  else
  { //parent process
    //calls sigHandler when a signal is received
    //otherwise pause()
    signal(SIGCONT, sigHandler);
    pause();
  }

  return 0;
}
