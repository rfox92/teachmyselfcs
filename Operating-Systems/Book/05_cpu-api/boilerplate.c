#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
  int x = 100;
  printf("hello, I am parent (pid:%d)\n", (int) getpid());

  int fork_rc = fork();
  if ( 0 > fork_rc )
  {
    fprintf(stderr, "fork failed\n");
    exit(1);
  }
  else if ( 0 == fork_rc )
  { //child process
    printf("  child pid:%d\n", (int) getpid());
  }
  else
  { //parent process
    printf("  parent pid:%d\n", (int) getpid());
  }

  return 0;
}
