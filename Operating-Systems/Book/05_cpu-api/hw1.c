#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
  int x = 100;
  printf("hello, I am parent (pid:%d)\n", (int) getpid());
  printf("value of x in parent:%d\n", x);
  int fork_rc = fork();
  if ( 0 > fork_rc )
  {
    fprintf(stderr, "fork failed\n");
    exit(1);
  }
  else if ( 0 == fork_rc )
  { //child process
    printf("  child pid:%d\n", (int) getpid());
    printf("  value of x in child:%d\n", x);
    printf("  incrementing x in child. New value: %d\n", ++x);
  }
  else
  { //parent process
    printf("  parent pid:%d\n", (int) getpid());
    printf("  value of x in parent:%d\n",x);
    printf("  incrementing x in parent. New value: %d\n", ++x);
  }

  return 0;
}
