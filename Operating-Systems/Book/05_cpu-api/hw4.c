#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

/*
      Write a program that calls fork() and then calls some 
      form of exec() to run the program /bin/ls. See if you 
      can try all of the variants of exec(), including 
      (on Linux) execl(), execle(), execlp(), execv(), 
      execvp(), and execvpe(). Why do you think there are so 
      many variants of the same basic call?

      Answer: https://stackoverflow.com/questions/5769734/what-are-the-different-versions-of-exec-used-for-in-c-and-c
*/

int main (int argc, char *argv[])
{
  int x = 100;
  printf("hello, I am parent (pid:%d)\n", (int) getpid());

  int fork_rc = fork();
  if ( 0 > fork_rc )
  {
    fprintf(stderr, "fork failed\n");
    exit(1);
  }
  else if ( 0 == fork_rc )
  { //child process
    printf("    we are in the child process post fork\n");
    printf("    child pid is :%d\n", (int) getpid());
    printf("    child calling /bin/ls\n");

    char *childArgs[2];
    childArgs[0] = strdup("/bin/ls");
    childArgs[1] = NULL;    //needs to be null terminated
    
    execvp(childArgs[0], childArgs);
  }
  else
  { //parent process
    
    printf("  we are in the parent process post fork\n");
    printf("  waiting for child to finish\n");
    waitpid(-1, 0, 0); //wait for all children
    printf("  child finished\n");
  }

  return 0;
}
