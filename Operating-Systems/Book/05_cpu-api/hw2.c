#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
  int x = 100;
  printf("hello, I am parent (pid:%d)\n", (int) getpid());

  printf("argc=%d\n", argc);

  if ( 2 > argc )
  { //check that we have enough arguments
    fprintf(stderr, "Error: please enter a file name\n");
    exit(1);
  }

  // close stdout, open what was specified in argv[1] instead
  close(STDOUT_FILENO);
  int fd = open(argv[1], O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|S_IWUSR);

  int fork_rc = fork();
  if ( 0 > fork_rc )
  {
    fprintf(stderr, "fork failed\n");
    exit(1);
  }
  else if ( 0 == fork_rc )
  { //child process
    printf("  child pid:%d\n", (int) getpid());
    printf("  this is the child writing to the file instead of stdout\n");
  }
  else
  { //parent process
    printf("  parent pid:%d\n", (int) getpid());
    printf("  this is the parent writing to the file instead of stdout\n");

  }

  return 0;
}
