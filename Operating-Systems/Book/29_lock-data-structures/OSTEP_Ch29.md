# Chapter 29: Lock Based Concurrent Data Structures

- Adding locks to a data structure is usually required to make it **thread safe**
- Lock implementation determines correctness and performance of the data structure
- The challenge then becomes:

For a given data structure, how/where do we add locks to make sure it works correctly? Secondly, how do we add locks such that the data structure has high performance (and thus suitable for a high degree of concurrency)?

## Concurrent Counter
- If we have a simple counter, and add a single lock around the increment/decrement/get operations, we have a correct data structure!
  - If this *does* provice ample performance, we're done! No sense optimizing prematurely!
- But this data structure will certainly take longer with more threads accessing in parallel
  - If they need to access often, contention will occur
  - Ideally, threads would complete the task just as quickly in a multithreaded configuration compared to a single threaded configuration (**perfect scaling**)
- One optimization might be to use an *approximate counter*. Each thread can keep its own local count (using its own thread-local lock), up to some `threshold`. Once the local thread counter reaches the threshold, it can grab the global lock and update the global count, resetting its local counter.
- This means that whenever `get()` is called  on the counter will differ from the "true" value by at most `threshold * numThreads`
- Smaller thresholds give more accurate results when retrieving the count, but also result in performance closer to the simple counter


### Approximate Counter
```C
typedef struct __counter_t {
    int             global;         // global count
    pthread_mutex_t glock;          // global lock
    int             local[NUMCPUS]; // local (per CPU) count
    pthread_mutex_t llock[NUMCPUS]; // local (per CPU) lock
    int             threshold;      // update frequency
}

// set our threshold, init all our locks and values
void init(counter_t *c, int threshold) {
    c->threshold = threshold;
    c->global = 0;
    pthread_mutex_init(&c->glock, NULL);
    for (int i = 0; i < NUMCPUS; i++){
        c->local[i] = 0;
        pthread_mutex_init(&c->llock[i], NULL);
    }
}

void update(counter_t *c, int threadID, int amount) {
    int cpu = threadID % NUMCPUS;
    pthread_mutex_lock(&c->llock[cpu]);
    c->local[cpu] += amt;
    if (c->local[cpu] >= c->threshold) { // if we're above the threshold on this local counter, 
        pthread_mutex_lock(&c->glock);
        c->global += c->local[cpu];     // increment the global count
        pthread_mutex_unlock(&c->glock);
        c->local[cpu] = 0;              // and reset our local counter
    }
    pthread_mutex_unlock(&c->llock[cpu]);
}

int count(counter_t *c) {   // just get the global count, which will be out
                            // by at most NUMCPUS*threshold
    pthread_mutex_lock(&c->glock);
    int val = c->global;
    pthread_mutex_unlock(&c->glock);
    return val;
}

```

- As threshold approaches 1, we approach the performance of a non-shared counter
- As threshold increases, we approach perfect scaling (but our `get()` call is less accurate)

#### More concurrency is not always faster
- Adding concurrency can result in increased overheads (e.g. introducing many locks) - the overhead inherent with the use of many locks may outweigh the performance benefits of increased concurrency
- Simple systems tend to work better, as they reduce overhead and are easier to use
- The only way to really know is to build both systems and measure!
  - Simple but less concurrent
  - Complex but more concurrent

#### Beware of locks and control flow!
- Concurrent code especially (though this is still relevant elsewhere) needs to be careful when managing application state
- Beware of early returns or other control flow changes
  - Many functions will begin by acquiring a lock, `malloc()`-ing or modifying state in some other way
  - On failure, we need to make sure we unwind the state before we return

## Concurrent Queues
- A common way to make queues more concurrent is to use two locks: one at the head of the queue and another at the tail
- the `enqueue()` operation will only need to acquire the tail lock
- the `dequeue()` operation will only need to acquire the head lock

```C
typedef struct __node_t {
    int value;
    struct __node_t *next;
}

typedef struct __queue_t {
    node_t          *head;
    node_t          *tail;
    pthread_mutex_t headLock;
    pthread_mutex_t tailLock;
}

void Queue_init(queue_t *q) {
    node_t *tmp = malloc(sizeof(node_t));   // allocate our dummy node
    tmp->next = NULL;
    q->head = q->tail = tmp;                // put our node at both the head and tail of the queue
    pthread_mutex_init(&q->headLock, NULL);
    pthread_mutex_init(&q->tailLock, NULL);
}

void Queue_enqueue(queue_t *q, int value) {
    node_t *tmp = malloc(sizeof(node_t));
    assert(tmp != NULL);                    // make sure malloc worked
    tmp->value = value;
    tmp->next = NULL;

    pthread_mutex_lock(&q->tailLock);
    q->tail->next = tmp;
    q->tail = tmp;
    pthread_mutex_unlock(&q->tailLock);
}

int Queue_dequeue(queue_t *q, int *val) {
    pthread_mutex_lock(&q->headLock);
    node_t *tmp = q->head;
    node_t *newHead = *tmp->next;
    if (newHead == NULL) {
        pthread_mutex_unlock(&q->headLock);
        return -1;                          // queue was empty
    }
    *value = newHead->value;                // is this right? why do we get the newHead->value?
                                            // I assume we really should be getting q->head->value...
    q->head = newHead;
    pthread_mutex_unlock(&q->headLock);
    free(tmp);
    return 0;
}
```