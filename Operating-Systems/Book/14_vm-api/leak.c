#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("    allocating 1 byte on the heap\n");
    void *mem = malloc(1);
    
    if (mem == NULL)
    {
        printf("    malloc failed, exiting\n");
        exit(1);
    }

    printf("    malloc succeeded. memory allocated at %p\n", mem);
    printf("    exiting without freeing memory\n");
}