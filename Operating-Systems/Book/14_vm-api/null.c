
#include <stdlib.h>
#include <stdio.h>


int main()
{
    printf("    creating a pointer to an integer\n");
    int x = 7;
    int *y = &x;
    printf("    incrementing the integer via the pointer\n");
    printf("    old value: %d.\n  ", (*y));
    printf("    new value: %d\n", ++(*y));
    
    printf("    setting the pointer to NULL\n");
    y = NULL;
    printf("    about to try and dereference the pointer\n");
    printf("    new value at pointer: %d\n", (*y)); // this segfaults! unsurprising
    printf("    press enter to exit\n");
    getchar();
}