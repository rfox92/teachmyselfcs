#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("    allocating an array of 8 integers on the heap");
    printf(" (%d bytes each)\n", sizeof(int));
    int *mem = (int *) malloc(8*sizeof(int));
    if (mem == NULL)
    {
        printf("    malloc failed, exiting\n");
        exit(1);
    }
    printf("    successfully allocated memory at %p\n", mem);
    printf("    data in array:\n");
    int i;

    for(i = 0; i < 8; i++)
    {
        printf("        arr[%d]:%d\n", i, mem[i]);
    }

    printf("    freeing memory\n");
    free(mem);

    printf("    data in array:\n");
    for(i = 0; i < 8; i++)
    {
        printf("        arr[%d]:%d\n", i, mem[i]);
    }

    printf("    exiting\n");
}