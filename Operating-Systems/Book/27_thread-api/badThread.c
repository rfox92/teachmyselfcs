#include <stdio.h>
#include <stdlib.h>
#include "../common.h"
#include "../common_threads.h"

typedef struct __myarg_t
{
    int a;
    int b;
} myarg_t;

typedef struct __myret_t
{
    int x;
    int y;
} myret_t;



void *mythread(void *arg)
{
    myarg_t *m = (myarg_t *) arg; //cast the input pointer to a pointer to myarg_t
    printf("%d %d\n", m->a, m->b);
    
    myret_t r; //bad - allocated on stack!
    r.x = 1;
    r.y = 2;

    return (void *) &r;
}


int main(int argc, char *argv[])
{
    pthread_t p;
    myret_t *m;
    myarg_t args = {10, 20};
    Pthread_create(&p, NULL, mythread, &args);

    Pthread_join(p, (void **) &m);
    printf("returned %d %d\n", m->x, m->y);

    free(m);
    
    return 0;
}