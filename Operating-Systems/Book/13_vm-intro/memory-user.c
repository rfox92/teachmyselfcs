#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <time.h>

#pragma region defines
//defines to get around vs code
#ifndef MADV_SEQUENTIAL
#define MADV_SEQUENTIAL 0
#endif//MADV_SEQUENTIAL

#ifndef MADV_WILLNEED
#define MADV_WILLNEED 0
#endif//MADV_WILLNEED

#ifndef MADV_RANDOM
#define MADV_RANDOM 0
#endif//MADV_RANDOM

#ifndef MADV_DONTNEED
#define MADV_DONTNEED 0
#endif//MADV_DONTNEED
#pragma endregion//defines

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Enter a number of Mb to allocate\n");
        exit(1);
    }

    long int num = atoi(argv[1]);

    if ( 0 == num)
    {
        printf("Enter a number of Mb to allocate\n");
        exit(1);
    }
    printf("my pid: %d\n", (int) getpid());
    printf("allocating %ld Mb\n", num);

    void *mem = malloc(num*1024*1024);

    time_t starttime = time(NULL);
    

    if (mem == NULL)
    {
        printf("malloc failed, exiting\n");
        exit(1);
    }
    printf("allocated %ld Mb\n", num);
    

    // tell the kernel we will be reading this memory sequentially
    // and that we will need it shortly
    if ((argc >= 3) && (0 == strncmp(argv[2], "m", 1)))
    {   
        printf("calling madvise for performance\n");
        madvise(mem, num*1024*1024, MADV_SEQUENTIAL | MADV_WILLNEED);
    }
    if ((argc >= 3) && (0 == strncmp(argv[2], "n", 1)))
    {
        printf("calling madvise for worse performance\n");
        madvise(mem, num*1024*1024, MADV_RANDOM | MADV_DONTNEED);
    }
    printf("zeroing the entire block of memory\n");

    long int current = 0;
    while (current < num*1024*1024)
    {
        // this segfaults if we malloc >= 2048 MB???
        // segfault because of integer overflow
        int *curr_int = mem + current;
        *curr_int = 0;
        current++;
    }

    time_t endtime = time(NULL);
    printf("zeroing complete.\n");
    
    //printf("zeroing start time: %ld\n", starttime);
    //printf("zeroing end time: %ld\n", endtime);
    printf("zeroing elapsed time: %ld\n", endtime - starttime);

    printf("press enter to exit (this frees the memory)\n");
    getchar();

    free(mem);
}
