#include <stdio.h>
#include <stdlib.h>

int main()
{
  void *code = (void *) main;
  void *heap = malloc(1);
  int x = 0;
  void *stack = &x;

  printf("location of code : %p\n", code);
  printf("location of heap : %p\n", heap);
  printf("location of stack: %p\n", stack);

  //printf("code length : %ld bytes\n", (heap - code));


  long int mb_constant = 1024*1024;
  long int gb_constant = mb_constant*1024;
  long int tb_constant = gb_constant*1024;

  // this seems to vary? why?

  printf("code length : %ld Mbytes\n", (heap - code)/(mb_constant));
 
  //printf("total length: %ld bytes\n", (stack - code));
  //printf("total length: %ld Mbytes\n", (stack - code)/(mb_constant));

  // why is this so large? I assume virtualization?
  // tends to be on the order of 40Tb??

  printf("total length: %ld Tbytes\n", (stack - code)/(tb_constant));
  return x;
}
