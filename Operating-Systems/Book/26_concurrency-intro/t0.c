#include <stdio.h>
#include <assert.h>
#include "common.h"
#include "common_threads.h"
#include <pthread.h>

void *mythread(void *arg) {
    printf("%s\n", (char *) arg) ;
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t p1, p2;
    int rc;
    printf("main: begin\n");
    //run our two threads
    rc = pthread_create(&p1, NULL, mythread, "p1"); assert(rc == 0);
    rc = pthread_create(&p2, NULL, mythread, "p2"); assert(rc == 0);
    //wait for our threads to finish
    rc = pthread_join(p1, NULL); assert(rc == 0);
    rc = pthread_join(p2, NULL); assert(rc == 0);
    //finish up
    printf("main: end\n");
    return 0;
}