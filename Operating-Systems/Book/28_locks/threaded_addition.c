#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

static int i = 0;

void *add_million()
{
    
    for (int j = 0; j < 1000000; j++)
    {
        i++;
    }
}

int main()
{
    pthread_t t1, t2;
    
    pthread_create(&t1, NULL, add_million, NULL);
    pthread_create(&t2, NULL, add_million, NULL);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    printf("Value of i after 2 threads incremented 1 million times: %i\n", i);
}