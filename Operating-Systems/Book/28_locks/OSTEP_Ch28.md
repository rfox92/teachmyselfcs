# Chapter 28: Locks

- One of the fundamental problems in concurrency is that we want to execute some series of instructions atomically, but interrupts and the OS are fighting against us!
- This chapter introduces the concept of a `lock`, which addresses this problem.

## Basic Idea
Assuming that our critical section looks something like this:

```C
balance = balance + 1
```

To use a lock around this critical section, we do the following:
```C
lock_t mutex;
...
lock(&mutex);
balance = balance + 1;
unlock(&mutex);
```

- The `mutex` variable above stores the current state of the lock. It can take value `{ locked | unlocked }`
- Calling `lock(&mutex)` attempts to transition `mutex` from `unlocked` to `locked`. If it is already `locked`, we wait until it is `unlocked` before we `lock` it.
- Once we acquire the lock, we can enter the critical section (incrementing our balance)
- If we have multiple variables that require protected access at different times. we can use multiple locks (a *fine-grained* locking strategy) or a single lock (a *coarse-grained* locking strategy)
- Fine-grained locking will generally perform better, at the cost of increased implementation complexity

## Building Locks
We now have the semantics of using locks, but how do you build them?

```
How do we build an effient lock? We need to provide mutual exclusion at low cost, and still be able to guarantee correct execution. What do we need from the hardware? What do we need from the OS?
```

## Lock Evaluation
What do we need from locks? What are our goals here?
  - Does the lock perform its basic function (`mutual exclusion`)?
  - Is the lock `fair`? Do all competing threads get an equal go at acquiring the lock once it is released?
  - What is the time overhead/`performance` impact of the lock?
    - Consider/compare the following cases:
      - Single thread acquiring/releasing the lock
      - Multiple threads acquiring/releasing the lock, executing on a single CPU
      - Multiple threads acquiring/releasing the lock, executing on multiple CPUs

## Disable Interrupts
- Simplest way to provide a locking mechanism is to disable interrupts - you can't get interrupted if they're disabled!

```C
void lock() { disableInterrupts(); }
void unlock() { enableInterrupts(); }
```

- Very heavy handed approach
- Only works on single core systems
- It requires the ability for any calling thread to have the permission to call a privileged system call and you need to trust that this permission will not be abused
  - Given we need to trust all arbitrary programs, we must be in a bit of trouble here
    - Greedy programs could call `lock()` on start and call `unlock()` on end, ensuring they have full control over the hardware
    - Poorly-written programs might call `lock()` and then go in to an endless loop
  - In either of the two examples, the OS never has the opportunity to take back control from the bad program
- Turning off all interrupts means that some interrupts might be lost (e.g. the CPU might miss the fact that a disk device has completed a read request)

As a result, this is used sparingly. An OS might use interrupt masking to implement its own concurrent data structures.  Because it's only happening in the OS, we don't have to trust arbitrary programs.

## Spin Locks with Test-And-Set
- We end up needing hardware support for locks if we want something better than disabling interrupts (which don't work on multiprocessor systems anyway)
- Simplest hardware support is the **test-and-set instruction**, or **atomic exchange**

```C
int testAndSet(int *p_old, int new) {
  int old = *p_old;
  *p_old = new;
  return old;
}
```

- The key here is that `testAndSet()` is performed atomically

```C
typedef struct __lock_t {
  int flag;
} lock_t;

void init(lock_t *lock) {
  // 0 indicates that the lock is free, 1 indicates that something holds the lock
  lock->flag = 0;
}

void lock(lock_t *lock) {
  // We test and set the lock, and if the value was already 1 (locked) then we wait
  while (testAndSet(&lock->flag, 1) == 1)
    ;
}

void unlock(lock_t *lock) {
  lock->flag = 0;
}
```

How does this lock work?

- If a thread calls `lock()`, and `flag == 0` then `testAndSet()` will set `flag = 1` and return 0, indicating that the lock *was* free but now the calling thread owns it. The calling thread will not enter the `while` loop, so it will not spin.
- If a thread calls `lock()`, and `flag == 1` then `testAndSet()` will set `flag = 1` again and return 1, indicating that the calling thread must wait to acquire the lock. Until the owner of the lock sets `flag = 0` the thread will continually `testAndSet()` and spin

### Spin Lock Evaluation
- These locks are called **spin locks** because they spin (using CPU cycles) until the lock is available
- How effective is the lock, based on our axes?
  - `correctness`: Yes! Only one thread can enter a critical section at a time
  - `fairness`: No... we can't guarantee that an arbitrary thread will enter a critical section. Some threads might starve
  - `performance`
    - Single thread acquiring/releasing the lock: overhead is basically the cost of the testAndSet instruction
    - Multiple threads on single CPU: multiple threads will be spinning, adding a pretty significant performance overhead
    - Multiple threads on multiple CPUs: if the number of threads roughly equals the number of CPUs, we have a minimal performance overhead

## Compare and Swap
- Compare and swap/compare and exchange (instructions on SPARC or x86, respectively) is another hardware-provided primitive we can use to build locks

```C
int compareAndSwap(int *ptr, int expected, int new) {
  int actual = *ptr;
  if (actual == expected) { *ptr = new; }
  return actual;
}
```

- Tests whether the value at `*ptr` is equal to the `expected`. If it is, we update it. If not, we do nothing. Crucially, we tell the caller what the actual value at `*ptr` was, so that it knows whether the operation succeeded or not.
- Our lock here ends up being almost identical to `testAndSet()`:
```C
void lock(lock_t *lock) {
  while (compareAndSwap(&lock->flag, 0, 1) == 1)
    ; //spin
}
```

- Compare and swap is a little more powerful than test and set - we can use it for *lock free synchronization* 
- If we use it to build a spin lock as above, the behaviour is identical to the test and set spin lock

## Load-Linked and Store-Conditional
- Some platforms have the load-linked and store-conditional instructions that can be used together to build critical sections

```C
int loadLinked(int *ptr) {
  return *ptr;
}

int storeConditional( int *ptr, int value) {
  if (/*no updates to *ptr since we loadLinked() to the address*/) {
    *ptr = value;
    return 1; // successfully updated
  } else {
    return 0; // failed to update
  }
}
```
Our lock ends up looking something like this
```C
void lock(lock_t *lock) {
  while (1) {
    while (loadLinked(&lock->flag) == 1)
      ; // spin while the lock is held by someone else
    if (storeConditional(&lock->flag, 1) == 1)
      return; // if we successfully grabbed the lock, we're done here
              // otherwise we need to try again
  }
}
void unlock(lock_t *lock) {
  lock->flag = 0;
}
```
## Fetch-and-Add
- Atomically increments a value at an address while returning the old value
```C
int fetchAndAdd(int *ptr) {
  int old = *ptr;
  *ptr = old + 1;
  return old;
}
```
- We need to use a slightly more complex lock, but we guarantee that we can't starve threads with this new (ticket based) lock!

```C
typedef struct __lock_t {
  int ticket;
  int turn;
} lock_t;

void lock_init(lock_t *lock) {
  lock->ticket = 0;
  lock->turn = 0;
}

void lock(lock_t *lock) {
  int myTurn = fetchAndAdd(&lock->ticket);
  while (lock->turn != myturn)
    ; // spin
}

void unlock (lock_t *lock) {
  lock->turn = lock->turn + 1;
}
```

- Each thread that wants the lock gets a place in the queue
- The lock remembers whose turn it is

## How do we deal with excess spinning?
- Hardware locks are simple and they can provide the guarantees we need
- Simple locks can be inefficient, though
- Consider two threads `t0` and `t1`:
  - `t0` holds a lock and is in a critical section
  - Timer interrupt goes off, `t0` is moved from `running` to `ready` state
  - `t1` is moved from `ready` to `running`, but is waiting on the lock held by `t1`
  - `t0` can't complete its critical section if it's not `running`!
  - `t1` will spin until the timer interrupt goes off again, taking up CPU time
- **Whenever a thread ends up spinning in a situation like this, it wastes an entire timer interrupt cycle checking a value that cannot change!**
- Problem is exaggerated when we have `N` threads contending for a lock: we may waste `N-1` time slices waiting for a single thread to release the lock
- We need something other than hardware support to solve this one...

## `yield()`
- If the OS provides us a `yield()` system call, we can prevent this!
  - The `yield()` primitive should allow a thread to tell the OS "please move me from `running` to `ready`"
    - Thread effectively deschedules itself
- Works well for 2 threads competing for a lock, but how about 100 threads?

## Queueing
- The underlying issue with our other solutions is that we have to work around the black box of the OS scheduling
- If the scheduler makes a bad choice (e.g. chooses a thread that is still waiting to acquire a lock from one of the `ready` threads), then the chosen thread needs to either spin until the next timer interrupt, or yield - in either case, we've at least had the overhead of two context switches for no real benefit
- To avoid this, we need some way to control the next thread to acquire the lock after the current holder releases it
  - This requires a little more OS support, including a queue that can keep track of which threads are waiting on a lock

- We need two calls from the OS: `park()` (which puts a calling thread to sleep) and `unpark(threadID)`, which allows one thread to wake another
- These two syscalls can be used to build a lock that puts the caller to sleep if it attempts to acquire a held lock, and wakes it when the lock is free

```C
typedef struct __lock_t {
  int flag;
  int guard;
  queue_t *q;
}

void lock_init(lock_t *m) {
  m->flag = 0;
  m->guard = 0;
  queue_init(m->q);
}

void lock(lock_t m*) {
  while(TestAndSet(&m->guard, 1) == 1)
    ;                   // get the guard by spinning
  if (m->flag == 0) {
    m->flag = 1;        // acquire the lock
    m_.guard = 0;
  } else {
    queue_add(m->q, gettid());
    m->guard = 0;
    park();
  }
}

void unlock(lock_t *m) {
  while(TestAndSet(&m->guard, 1) == 1)
    ;                 // get the guard by spinning
  if (queue_empty(m->q))
    m->flag = 0;      // release the lock, don't wake anyone else up because no one cares
  else
    unpark(queue_remove(m->q));  // hold the lock, hand it to the next thread
  m->guard = 0;
}
```

- Using an explicit queue of lock waiters ensures that no thread ever gets starved - threads will each acquire the resource in the order that they request it
- Threads might still get stuck spin waiting if a thread gets interrupted while acquiring or releasing the lock, but this should occur rarely as the time spent inside that code is relatively small 
- If a thread can't get the lock, it adds itself to the queue

## Linux

- Solaris uses `park()` and `unpark(pid)`, but there are other options
- Linux provides a `futex`, similar to the Solaris interface but does more things in-kernel
- Each `futex` is associated with a specific physical memory location and an in-kernel queue
- The semantics are:
  - `futex_wait(address, expected);` puts the calling thread to sleep, assuming that `*address == expected`
    - If `*address != expected`, the call returns immediately
  - `futex_wake(address);` wakes one thread waiting on the queue












